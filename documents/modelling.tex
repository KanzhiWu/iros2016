\documentclass{article}

\usepackage{amsmath,amsfonts,amssymb}
\usepackage{multicol}
\usepackage{multirow}
\usepackage{graphicx}
\usepackage[titletoc,toc,title]{appendix}

\newcommand\undermat[2]{%
	\makebox[0pt][l]{$\smash{\underbrace{\phantom{%
					\begin{matrix}#2\end{matrix}}}_{\text{$#1$}}}$}#2}

\begin{document}

\section*{Problems - 06/11/2015}
In objective function eq.\ref{eq::obj_func}, the $ 1 $st term counts the uncertainty in robot pose and the $ 2 $nd term counts: 1) the number of observable features; 2) the repeatability of observable features. The $ 1 $st term, according to eq.\ref{eq::posterior_cov}, the random assumption of possible observation is not considered. However, in the $ 2 $nd term, $ \Upsilon $ is a function of the mean of posterior $ X^{\Delta}_{k+L-1} $ and also according to eq.\ref{eq::posterior_mean}, $ X^{\Delta}_{k+L} $ is a function of control $ u_{k:k+L-1} $ and random observation $ z_{k+i,j} $ (in eq.\ref{eq::rand_observation}). In Vadim's work, in Appendix B, the expectation eliminates the random variable, however, in our problem, the function $ l(\mathbb{E}(\Upsilon)) $ is nonlinear, and I cannot see a possible way to eliminate the random variable.

The current solution I can figure out now is: assume the observation to be accurate, therefore the problem can be solved. Here my consideration is that the problem is over-simplified and I am not sure it is the suitable way to continue.

Also, code up to this point is finished.

\section*{Notations}
	
\subsection*{State}
$ X_{i} $ is the state vector in $ k $-th step includes current state estimation of the object $ O $ (given only $ 1 $ object presents) and state of $ k $ steps of robot poses $ X^{p}_{1:k} $.
\begin{equation}
X_{i}  = 
\left[
\begin{array}{c}
X^{o} \\ X^{p}_{1} \\ \vdots \\ X^{p}_{i}
\end{array} 
\right],
\quad
X^{o} = \left[ \begin{array}{c}
x^{o} \\ y^{o} \\ \theta^{o}
\end{array} \right],
\quad
X^{p}_{i} = \left[\begin{array}{c}
x^{p}_{i} \\ y^{p}_{i} \\ \theta^{p}_{i}
\end{array}\right]
\end{equation}

\subsection*{Observation}
Given the assumption of using RGB-D sensor, the observation vector $ Z $ is the coordinates of observed features $ p_{i} $ in sensor coordinate frame. In simulation, under 2D environment, $ y $ coordinate in $ p_{i} $ is ignored.
\begin{equation}
\mathbf{Z}_{i} = \left[\begin{array}{c}
Z_{1} \\ Z_{2} \\ \vdots \\ Z_{i}
\end{array}\right],
\quad
Z_{i} = \left[\begin{array}{c}
p_{i,1} \\ p_{i,2} \\ \vdots \\ p_{i,n_{i}}
\end{array}\right],
\quad
p_{i,j} = \left[\begin{array}{c}
x_{i,j} %\\ y_{i,j}
 \\ z_{i,j}
\end{array}\right]
\end{equation}

\subsection*{Control}
The control variables $ u $ are angular velocity $ \omega $ and linear velocity $ \nu $. Time step is denoted as $ \delta t $.

\section*{Models}

\subsection*{Motion Models $ \mathbf{G} $}
\begin{align}
X^{p}_{i+1} &= 
\mathbf{G}\left(X^{p}_{i}, u_{i}\right) + \eta \\
\left[\begin{array}{c}
x^{p}_{i+1} \\ y^{p}_{i+1} \\ \theta^{p}_{i+1}
\end{array}\right] &=
\left[\begin{array}{c}
x^{p}_{i} \\ y^{p}_{i} \\ \theta^{p}_{i}
\end{array}\right]　
+
\left[\begin{array}{r}
\nu \delta t \cdot \cos(\theta^{p}_{i}+\omega \delta t) \\
\nu \delta t \cdot \sin(\theta^{p}_{i}+\omega \delta t) \\
\omega \delta t
\end{array}\right] + \eta
\end{align}
where $ \eta \sim \mathcal{N}(0, \Omega_{\eta}) $

\subsubsection*{EKF Prediction}
\begin{equation}
\mathbf{G}_{X} = 
\dfrac{\partial \mathbf{G}}{\partial X} = 
\left[\begin{array}{ccr}
1 & 0 & -\nu \delta t \cdot \sin(\theta^{p} + \omega \delta t) \\
0 & 1 & \nu \delta t \cdot \cos(\theta^{p} + \omega \delta t) \\
0 & 0 & 1 \\
\end{array}
\right]
\end{equation}
\begin{equation}
\mathbf{G}_{u} = 
\dfrac{\partial \mathbf{G}}{\partial u} = 
\left[\begin{array}{rr}
\delta t \cdot \cos(\theta^{p}+\omega \delta t) & -\nu \delta t^{2} \cdot \sin(\theta^{p}+\omega \delta t) \\
\delta t \cdot \sin(\theta^{p}+\omega \delta t) & \nu \delta t^{2} \cdot \cos(\theta^{p}+\omega \delta t) \\
0 & \delta t\\
\end{array}\right]
\end{equation}
Covariance matrix is updated using 
\begin{equation}
\bar{\Sigma}_{i+1} = \mathbf{G}_{X}\Sigma_{i}\mathbf{G}^{\intercal}_{X} + \mathbf{G}_{u}\mathbf{Q}\mathbf{G}^{\intercal}_{u}
\end{equation}
where $ \mathbf{Q} $ maps the control noise to state space.

\subsection*{Observation Model $ \mathbf{H} $}
\begin{equation}
Z_{i} = \mathbf{H}(\mathbf{X}_{i}) + \xi
\label{eq::obsori}
\end{equation}
where $ \xi \sim \mathcal{N}(0, \Omega_{\xi}) $. In fact, $ Z_{i} $ only relates with the current estimated pose of object $ X^{o} $ and current pose of sensor $ X^{p}_{i} $ and eq.\ref{eq::obsori} is simplified as:
\begin{align}
Z_{i} &= \mathbf{H}(X^{o}, X^{p}_{i}) + \xi \\ 
&= \mathbf{H}(x^{o}, y^{o}, \theta^{o}, x^{p}_{i}, y^{p}_{i}, \theta^{p}_{i}) + \xi 
\end{align}
Given feature point $ f_{j}|_{o} = \left(x^{f}_{j}|_{o}, y^{f}_{j}|_{o}\right)$ in object coordinate frame (in object modelling step), the observation function is further detailed as:
\begin{equation}
Z_{i} = \left[\begin{array}{c}
z_{i,1} \\ z_{i,2} \\ \vdots \\ z_{i,n_{j}}
\end{array}\right] \text{ where }
z_{i,j} = \left[\begin{array}{c}
u_{i,j} \\ v_{i,j}
\end{array}\right]
\end{equation}
\begin{equation}
z_{i,j} = h\left(
X^{o}, X^{p}_{i}, x^{f}_{i}|_{o}, y^{f}_{i}|_{o}
\right) + \xi_{j}
\end{equation}

\begin{equation}
\left[\begin{array}{c}
u_{i, j} \\d_{i, j} \\ 1
\end{array}\right] = 
\mathbf{G}|_{pw}\mathbf{G}|_{wo}
\left[\begin{array}{c}
x^{f}_{j}|_{o} \\ y^{f}_{j}|_{o} \\ 1
\end{array}\right] + \xi_{j}
\label{eq::obs}
\end{equation}
where $ \mathbf{G}|_{wo} $ transforms feature $ f_{j}|_{o} $ into world coordinate frame and $ \mathbf{G}|_{pw} $ transforms features $ f_{j}|_{w} $ into object pose coordinate frame. % and $ \mathbf{F}_{c} $ is the intrinsic matrix of the camera.
\begin{align}
\mathbf{G}|_{wo} &= \left[\begin{array}{cc}
\mathbf{R}_{wo} & \mathbf{t}_{wo} \\
0 & 1
\end{array}\right] \\
&= \left[\begin{array}{rrr}
\cos\theta^{o} & \sin\theta^{o} & x^{o} \\
-\sin\theta^{o} & \cos\theta^{o} & y^{o} \\
0 & 0 & 1
\end{array}\right] \\
\mathbf{G}|_{pw} &= (\mathbf{G}|_{wp})^{-1} \\
&= \left[\begin{array}{rrr}
\cos\theta^{p}_{i} & \sin\theta^{p}_{i} & x^{p}_{i} \\
-\sin\theta^{p}_{i} & \cos\theta^{p}_{i} & y^{p}_{i} \\
0 & 0 & 1
\end{array}\right]^{-1}\\
&= \left[\begin{array}{rrr}
\cos\theta^{p}_{i} & -\sin\theta^{p}_{i} & -(\cos\theta^{p}_{i}x^{p}_{i}-\sin\theta^{p}_{i}y^{p}_{i}) \\
\sin\theta^{p}_{i} & \cos\theta^{p}_{i} & -(-\sin\theta^{p}_{i}x^{p}_{i}+\cos\theta^{p}_{i}y^{p}_{i}) \\
0 & 0 & 1
\end{array}\right]
\label{eq::G}
\end{align}
Taking eq.\ref{eq::G} into eq.\ref{eq::obs},we get
\begin{align}
\left[\begin{array}{c}
x^{f}_{j}|_{X^{p}_{i}} \\ y^{f}_{j}|_{X^{p}_{i}} \\ 1
\end{array}\right] &=
\mathbf{G}|_{pw}\mathbf{G}|_{wo}
\left[\begin{array}{c}
x^{f}_{j}|_{o} \\ y^{f}_{j}|_{o} \\ 1
\end{array}\right] \\
&= \left[\begin{array}{rrr}
\cos\theta^{p}_{i} & -\sin\theta^{p}_{i} & -(\cos\theta^{p}_{i}x^{p}_{i}-\sin\theta^{p}_{i}y^{p}_{i}) \\
\sin\theta^{p}_{i} & \cos\theta^{p}_{i} & -(-\sin\theta^{p}_{i}x^{p}_{i}+\cos\theta^{p}_{i}y^{p}_{i}) \\
0 & 0 & 1
\end{array}\right]\\
& \text{ }\quad 
\left[\begin{array}{rrr}
\cos\theta^{o} & \sin\theta^{o} & x^{o} \\
-\sin\theta^{o} & \cos\theta^{o} & y^{o} \\
0 & 0 & 1
\end{array}\right]
\left[\begin{array}{c}
x^{f}_{j}|_{o} \\ y^{f}_{j}|_{o} \\ 1
\end{array}\right] + \xi_{j}\notag 
\end{align}

\begin{align}
\left[\begin{array}{c}
x^{f}_{j}|_{X^{p}_{i}} \\ y^{f}_{j}|_{X^{p}_{i}} \\ 1
\end{array}\right] 
&=
\left[ \begin{array}{rr}
\cos\theta^{p}_{i}\cos\theta^{o}+\sin\theta^{p}_{i}\sin\theta^{o} & \cos\theta^{p}_{i}\sin\theta^{o}-\sin\theta^{p}_{i}\cos\theta^{o} \\
\sin\theta^{p}_{i}\cos\theta^{o}-\cos\theta^{p}_{i}\sin\theta^{o} & \sin\theta^{p}_{i}\sin\theta^{o}-\cos\theta^{p}_{i}\cos\theta^{o} \\
0 & 0
\end{array}\right. \\
& \qquad
\left. \begin{array}{r}
\cos\theta^{p}_{i}x^{o}-\sin\theta^{p}_{i}y^{o} -\cos\theta^{p}_{i}x^{p}_{i}+\sin\theta^{p}_{i}y^{p}_{i} \\ 
\sin\theta^{p}_{i}x^{o}+\cos\theta^{p}_{i}y^{o} +\sin\theta^{p}_{i}x^{p}_{i}-\cos\theta^{p}_{i}y^{p}_{i} \\
1
\end{array}\right] 
\left[\begin{array}{c}
x^{f}_{j}|_{o} \\ y^{f}_{j}|_{o} \\ 1
\end{array}\right]+\xi_{j}
\notag
\end{align}

\subsection*{Jacobian Matrices}
\begin{equation}
\dfrac{\partial h}{\partial \mathbf{X}_{i}} = \left[\begin{array}{rrrrrr}
\dfrac{\partial h_{x}}{\partial x^{o}} & \dfrac{\partial h_{x}}{\partial y^{o}} & \dfrac{\partial h_{x}}{\partial \theta^{o}} & \dfrac{\partial h_{x}}{\partial x^{p}_{i}} & \dfrac{\partial h_{x}}{\partial y^{p}_{i}} & \dfrac{\partial h_{x}}{\partial \theta^{p}_{i}} \\
\dfrac{\partial h_{y}}{\partial x^{o}} & \dfrac{\partial h_{y}}{\partial y^{o}} & \dfrac{\partial h_{y}}{\partial \theta^{o}} & \dfrac{\partial h_{y}}{\partial x^{p}_{i}} & \dfrac{\partial h_{y}}{\partial y^{p}_{i}} & \dfrac{\partial h_{y}}{\partial \theta^{p}_{i}} 
\end{array}\right]
\end{equation}

The detailed Jacobian matrices are:
\begin{align}
\dfrac{\partial h_{x}}{\partial x^{o}} &= \cos\theta^{p}_{i} \\
\dfrac{\partial h_{x}}{\partial y^{o}} &= -\sin\theta^{p}_{i} \\
\dfrac{\partial h_{x}}{\partial \theta^{o}} &= x^{f}_{j}|_{o}(\cos\theta^{o}\sin\theta^{p}_{i}-\cos\theta^{p}_{i}\sin\theta^{o}) + \\ & \qquad y^{f}_{j}|_{o}(\cos\theta^{o}\cos\theta^{p}_{i}+\sin\theta^{o}\sin\theta^{p}_{i}) \notag \\
\dfrac{\partial h_{x}}{\partial x^{p}_{i}} &= -\cos\theta^{p}_{i} \\
\dfrac{\partial h_{x}}{\partial y^{p}_{i}} &= \sin\theta^{p}_{i} \\
\dfrac{\partial h_{x}}{\partial \theta^{p}_{i}} &= -y^{f}_{j}|_{o}(\cos\theta^{o}\sin\theta^{p}_{i}-\cos\theta^{p}_{i}\sin\theta^{o}) \notag \\ & \qquad
-x^{f}_{j}|_{o}(\cos\theta^{o}\sin\theta^{p}_{i}-\cos\theta^{p}_{i}\sin\theta^{o}) \notag \\ & \qquad
+ y^{p}_{i}\cos\theta^{p}_{i} -y^{o}\cos\theta^{p}_{i} - x^{o}\sin\theta^{p}_{i} + x^{p}_{i}\sin\theta^{p}_{i} \\
% xfo*(cos(tho)*sin(thp) - cos(thp)*sin(tho)) + yfo*(cos(tho)*cos(thp) + sin(tho)*sin(thp))
% yp*cos(thp) - yfo*(cos(tho)*cos(thp) + sin(tho)*sin(thp)) - yo*cos(thp) - xfo*(cos(tho)*sin(thp) - cos(thp)*sin(tho)) - xo*sin(thp) + xp*sin(thp)
\dfrac{\partial h_{x}}{\partial x^{o}} &= \sin\theta^{p}_{i} \\
\dfrac{\partial h_{x}}{\partial y^{o}} &= \cos\theta^{p}_{i} \\
\dfrac{\partial h_{x}}{\partial \theta^{o}} &= 
y^{f}_{j}|_{o}(\cos\theta^{o}\sin\theta^{p}_{i}-\cos\theta^{p}_{i}\sin\theta^{o}) -\\ & \qquad
x^{f}_{j}|_{o}(\cos\theta^{o}\cos\theta^{p}_{i}+\sin\theta^{o}\sin\theta^{p}_{i}) \notag \\
\dfrac{\partial h_{x}}{\partial x^{p}_{i}} &= \sin\theta^{p}_{i} \\
\dfrac{\partial h_{x}}{\partial y^{p}_{i}} &= -\cos\theta^{p}_{i} \\
\dfrac{\partial h_{x}}{\partial \theta^{p}_{i}} &= 
y^{f}_{j}|_{o}(\cos\theta^{o}\sin\theta^{p}_{i}-\cos\theta^{p}_{i}\sin\theta^{o}) \notag \\ & \qquad
- x^{f}_{j}|_{o}(\cos\theta^{o}\cos\theta^{p}_{i}+\sin\theta^{o}\sin\theta^{p}_{i})\notag \\ & \qquad
+ x^{o}\cos\theta^{p}_{i} + x^{p}_{i}\cos\theta^{p}_{i} - yo\sin\theta^{p}_{i} + y^{p}_{i}\sin\theta^{p}_{i}
% yfo*(cos(tho)*sin(thp) - cos(thp)*sin(tho)) - xfo*(cos(tho)*cos(thp) + sin(tho)*sin(thp))
% xfo*(cos(tho)*cos(thp) + sin(tho)*sin(thp)) - yfo*(cos(tho)*sin(thp) - cos(thp)*sin(tho)) + xo*cos(thp) + xp*cos(thp) - yo*sin(thp) + yp*sin(thp)
\end{align}

\section*{General Belief Space}
Given steps range within $ l\in \left[1, L\right] $, the \textit{generalised belief} at $ l $th step is denoted as:
\begin{equation}
gb\left(X_{k+l}\right) \doteq p\left(X_{k+l}|\mathbf{Z}_{k}, \mathbf{u}_{k}, Z_{k+1:k+l}, u_{k:k+l-1} \right)
\label{eq::gb}
\end{equation}
The belief $ gb\left(X_{k+1}\right) $ is represented by Gaussian
\begin{equation}
gb\left(X_{k+l}\right) \sim \mathcal{N}\left(X^{\Delta}_{k+l}, \Sigma_{k+l}\right)
\end{equation}
The mean $ X^{\Delta}_{k+l} $ is set to be the \textit{maximum a posterior} (MAP)
\begin{equation}
X^{\Delta}_{k+l} = \underset{X_{k+l}}{\arg\max}gb(X_{k+l}) = \underset{X_{k+l}}{\arg\min}\left(-\log gb\left(X_{k+l}\right)\right)
\end{equation}
%The general objective function is formulated as
%\begin{equation}
%J_{k}\left(u_{k:k+L-1}\right)\doteq \underset{Z_{k+1:k+L}}{\mathbb{E}}\left\lbrace \sum_{l=1}^{L} f\left(gb\left(X_{k+l}\right)\right)\right\rbrace + \sum_{l=1}^{L}g\left(X_{k+l}\right)
%\end{equation}
%The objective function w.r.t. state estimation is designed as
%\begin{equation}
%f\left(gb\left(X_{k+l}\right)\right) = tr \left(M_{\Sigma}\Sigma^{-1}_{k+l}M^{\intercal}_{\Sigma}\right)
%\end{equation}
The optimal $ u^{\Delta}_{k:k+L-1} $ is computed as
\begin{equation}
u^{\Delta}_{k:k+L-1} \doteq \left\lbrace u^{\Delta}_{k}, \dots, u^{\Delta}_{k+L-1} \right\rbrace = \underset{u_{k:k+L-1}}{\arg\min}J_{k}\left(u_{k:k+L-1}\right)
\end{equation}

\section*{Optimisation}
\subsection*{Outer Layer}
The optimisation is solved using gradient method and the control is updated as
\begin{equation}
u^{(i+1)}_{k:k+L-1} = u^{(i)}_{k:k+L-1} - \lambda \nabla J_{k}
\end{equation}
Given current update $ u^{(i)}_{k:k+L-1} $, we evaluate the objective function $ J_{k} $. Calculate the gradient numerically by adding noises $ \delta u $ to each step from control input $ u_{k:k+L-1} $. $ \delta u $ is initialised as $ \mathbf{0} $, in our problem, $ \nu $ and $ \omega $ for $ L-1 $ steps are initialised as $ 0 $. Starting from step $ k $,  firstly add noise $ \epsilon $ to $ \delta u_{k} $, noisy control input is computed as 
\begin{equation}
\hat{u}_{k:k+L-1} = u_{k:k+L-1} + \delta u_{k}
\end{equation}
The objective function is again evaluated using $ \hat{u}_{k:k+L-1} $ and denoted as $ \hat{J}_{k} $ and $ \nabla J_{k} $ at $ k $th control is computed as $ \left(\hat{J}_{k}-J_{k}\right)/\epsilon $. Through adding noise to $ k $th step control to $ k+L-1 $th step control, $ \nabla J_{k}$ is obtained and 
\begin{equation}
u^{(i+1)}_{k:k+L-1} = u^{(i)}_{k:k+L-1} - \lambda \nabla J_{k}
\end{equation}
\subsection*{Inner Layer}
By adding another binary random variable $ \gamma_{i,j} $ for each observation $ z_{i,j} $, the joint probability density is
\begin{equation}
p\left(X_{k+l}, \Gamma_{k+1:k+l}, Z_{k+1:k+l}|\mathbf{Z}_{k}, \mathbf{u}_{k-1}, u_{k:k+l-1}\right)
\end{equation}
where $ \Gamma_{i} \doteq \left\lbrace \gamma_{i,j} \right\rbrace^{n_{i}}_{j=1} $. Given eq.\ref{eq::gb}, we marginalise on the latent variables $ \Gamma_{k+1:k+l} $ and get
\begin{align}
gb\left(X_{k+l}\right) &= p\left(X_{k+l}|\mathbf{Z}_{k}, \mathbf{u}_{k-1}, Z_{k+1:k+l}, u_{k:k+l-1}\right) \\
&= \sum_{\Gamma_{k+1:k+l}}p\left(X_{k+l}, \Gamma_{k+1:k+l}| \mathbf{Z}_{k}, \mathbf{u}_{k-1}, Z_{k+1:k+l}, u_{k:k+l-1} \right)
\end{align}
And the MAP estimate is computed as 
\begin{align}
X^{\Delta}_{k+l} &= \underset{X_{k+l}}{\arg\min}\underset{\Gamma_{k+1:k+l}|\bar{X}_{k+l}}{\mathbb{E}} \\
& \left[ -\log p \left( X_{k+l}, \Gamma_{k+l:k+l} | \mathbf{Z}_{k}, \mathbf{u}_{k-1}, Z_{k+1:k+l}, u_{k:k+l-1} \right) \right]
\label{eq::xdelta}
\end{align}
where $ \bar{X}_{k+l} $ is a given nominal state. We decompose eq.\ref{eq::xdelta} using chain rule
\begin{flalign}
& p \left( X_{k+l}, \Gamma_{k+l:k+l} | \mathbf{Z}_{k}, \mathbf{u}_{k-1}, Z_{k+1:k+l}, u_{k:k+l-1} \right) && \\ \nonumber
& \propto p(X_{k}|\mathbf{Z}_{k}, \mathbf{u}_{k-1}) \prod_{i=1}^{l}\left[ p\left(X_{k+i}|X_{k+i-1}, u_{k+i-1}\right)p\left( Z_{k+i}, \Gamma_{k+i} | X_{k+i} \right) \right] &&
\end{flalign}
The observation term with $ n_{i} $ observed features can be further expanded as using feature $ f_{j}|_{o} $
\begin{equation}
\prod_{j=1}^{n_{i}} p\left(z_{k+i,j}, \gamma_{k+i,j}| X_{k+i}\right) = \prod_{j=1}^{n_{i}} \left[ p\left(z_{k+i,j}|X_{k+i}, \gamma_{k+i,j}\right) p\left(\gamma_{k+i,j}|X_{k+i}\right) \right] 
\end{equation}

\begin{figure}[htbp]
	\caption{Feature observability model}
	\centering
	\includegraphics[width=8cm]{f_model.eps}
	\label{fig:fobs}
\end{figure}
\subsubsection*{Feature Observability Model}
We further model the feature observability $ \gamma_{r,j} = \hbar_{j}(X_{i})$ as Fig.\ref{fig:fobs}. Here we consider 2 properties of the feature:
\begin{itemize}
	\item Feature scale variance: we assume the $ \gamma $ shows the maximum response on distance $ 0.5 $ and shows Gaussian distribution within the change of the distance.
	\item Feature rotation variance: the feature is assumed to be re-captured with the angle of $ 45^{\circ} $ w.r.t. the local normal direction of the feature and also shows Gaussian distribution within the range.
\end{itemize}
The observability value $ \gamma $ is the product of $ \gamma_{d} $ and $ \gamma_{o} $:
\begin{align}
\gamma &= \gamma_{d}\gamma_{o} \\
\gamma_{d} &= \frac{1}{\sigma_{\gamma_{d}}\sqrt{2\pi}}\exp(-\frac{(d_{f}-\mu_{\gamma_{d}})^{2}}{2\sigma_{\gamma_{d}}}) \\
\gamma_{o} &= \frac{1}{\sigma_{\gamma_{o}}\sqrt{2\pi}}\exp(-\frac{(o_{f}-\mu_{\gamma_{d}})^{2}}{2\sigma_{\gamma_{o}}})
\end{align}
Where $ \mu_{\gamma_{d}} = 0.5 $ and $ \mu_{\gamma_{o}} = \pi/4 $.

\subsubsection*{Compute $ X^{\Delta}_{k+l} $}
Eq.\ref{eq::xdelta} is represented as

\begin{align}
X^{\Delta}_{k+l} &= \underset{X_{k+l}}{\arg\min}||X_{k}-X^{\Delta}_{k}||^{2}_{\Sigma_{k}} \\ \nonumber
&+ \sum_{i=1}^{l}||X_{k+i}-\mathbf{G}(X_{k+i-1}, u_{k+i-1})||^{2}_{\Omega_{\eta}} \\ \nonumber
&+ \sum_{i=1}^{l}\sum_{j=1}^{n_{i}}\hbar_{j}(X_{k+i})||z_{k+i,j}-h(X_{k+i},f_{j}|_{o})||^{2}_{\Omega_{\xi}} \\ \nonumber
&= \underset{X_{k+l}}{\arg\min}||X_{k}-X^{\Delta}_{k}||^{2}_{\Sigma_{k}} \\ \nonumber 
&+ \sum_{i=1}^{l}||X_{k+i}-\mathbf{G}(X_{k+i-1}, u_{k+i-1})||^{2}_{\Omega_{\eta}} \\  \nonumber
&+ \sum_{i=1}^{l}\sum_{j=1}^{n_{i}}||z_{k+i,j}-h(X_{k+i},f_{j}|_{o})||^{2}_{\bar{\Omega}_{\xi}} \label{eq::xdeltamin}
\end{align}


Where we define
\begin{equation}
\bar{\Omega}^{ij}_{\xi} = \hbar(X_{k+i})\Omega^{ij}_{\xi}
\end{equation}
The standard method to solve the above eq.\ref{eq::xdeltamin} is Gaussian-Newton gradient optimisation. We first linearise on the linearisation point $ \bar{X}_{k+l}(u_{k:k+l-1}) $. Using the linearisation point, eq.\ref{eq::xdeltamin} is converted into
\begin{align}
& \underset{\Delta X_{k+l}}{\arg\min}||\Delta X_{k}||^{2}_{\Sigma_{k}}+\sum_{i=1}^{l}||\Delta X_{k+i} - \frac{\partial \mathbf{G}}{\partial X_{k+i-1}} \Delta X_{k+i-1}||^{2}_{\Omega_{\eta}} \\ \nonumber
&+ \sum_{i=1}^{l}\sum_{j=1}^{n_{i}}||\frac{\partial h_{j}}{\partial X_{k+i}}\Delta X_{k+i} - b^{h}_{i,j}(z_{k+i,j})||^{2}_{\bar{\Omega}^{ij}_{\xi}}
\label{eq::lin_argmin}
\end{align}
In order to convert eq.\ref{eq::lin_argmin} into quadratic form, here we use
\begin{align}
\text{Given } x, y & \qquad ||x||^{2}_{\Omega_{x}} + ||y||^{2}_{\Omega_{y}} = \left\|\left(\begin{array}{c}
x \\ y
\end{array}\right)\right\|^{2}_{{\scriptsize \left(\begin{array}{cc}
\Omega_{x} & 0 \\ 0 & \Omega_{y}
\end{array}\right)}} \\
& \sum_{j}^{n}\left\|A_{j}x-b_{j}\right\|^{2}_{\Omega_{j}} = \left\|\mathbf{A}x-\mathbf{b}\right\|^{2}_{{\tiny \left(\begin{array}{ccc}
	\Omega_{1} & & \\ & \ddots & \\ & & \Omega_{n}
	\end{array}\right)}}
\end{align}

Starting from the $ 1 $st term in eq.\ref{eq::lin_argmin}
\begin{align}
\left\| \Delta X_{k} \right\|^{2}_{\Sigma_{k}} &= (\Delta X_{k})^{\intercal} \Sigma_{k} \Delta X_{k} \\ \nonumber
&= (\Delta X_{k})^{\intercal} \Sigma^{1/2}_{k} \Sigma^{1/2}_{k} \Delta X_{k} \\ \nonumber
&= (\Sigma^{1/2}_{k} \Delta X_{k} )^{2}
\end{align}
For the $ 2 $nd term
\begin{align}
\sum_{i=1}^{l}\left\| \Delta X_{k+i} - \mathbf{G}'\Delta X_{k+i-1} \right\|^{2}_{\Omega_{\eta}} &= \sum_{i=1}^{l}\left\| \Omega^{1/2}_{\eta} \Delta X_{k+i} - \Omega^{1/2}_{\eta} \mathbf{G}' \Delta X_{k+i-1} \right\| \\ \nonumber
& = \mathcal{G}\Delta \mathbf{X}
\end{align}

where $ \mathbf{G}'_{i} $ is the short for $ \frac{\partial \mathbf{G}}{\partial X_{k+i-1}} $ and $ \mathcal{G} $ is
\begin{equation}
\mathcal{G} = \left( \begin{array}{cccc|ccccc}
0 & \cdots & \cdots &  -\Omega^{1/2}_{\eta}\mathbf{G}_{1} & \Omega^{1/2}_{\eta} & 0 & \cdots & 0 & 0\\
0 & \cdots & \cdots & 0 & -\Omega^{1/2}_{\eta}\mathbf{G}_{2} & \Omega^{1/2}_{\eta} & \cdots & 0 & 0 \\
\vdots & \vdots & \vdots & \vdots & 0 & \ddots & \ddots & 0 & 0 \\
\vdots & \vdots & \vdots & \vdots & 0 & 0 & \ddots & \ddots & 0 \\
\undermat{3(k+1)}{0 & \cdots & \cdots & 0 }& \undermat{3l}{0 & 0 & 0&-\Omega^{1/2}_{\eta}\mathbf{G}_{l-1} & \Omega^{1/2}_{\eta}}
\end{array} \right)_{3(l-1)\times 3(k+l+1)}
\label{eq::G}
\end{equation}
and $ \Delta \mathbf{X} $ is
\begin{equation}
\Delta \mathbf{X} = \left( \begin{array}{c}
\Delta X^{o} \\ \left( \begin{array}{c}
X^{p}_{1} \\ \vdots \\ \Delta X^{p}_{k}
\end{array} \right) \\ \left( \begin{array}{c}
\Delta X^{p}_{k+1} \\ \vdots \\ \Delta X^{p}_{k+l}
\end{array} \right)
\end{array} \right)_{3(k+l+1)\times1}
\end{equation}

In the end, the last term which is the observation function can be detailed as
\begin{align}
& \sum_{i=1}^{l} \left( \sum_{j=1}^{n_{i}}\left\| h'_{j}\Delta X_{k+i} - b^{h}_{i,j}(z_{k+i,j})\right\|^{2}_{\bar{\Omega}^{ij}_{\xi}} \right) && \\ \nonumber 
&=\sum_{i=1}^{l} \left( \sum_{j=1}^{n_{i}}\left\| (\bar{\Omega}^{ij}_{\xi})^{1/2}h'_{j} \left( \begin{array}{c}
\Delta X^{o} \\ \Delta X^{p}_{k+i}
\end{array} \right) \Delta X_{k+i} - (\bar{\Omega}^{ij}_{\xi})^{1/2}b^{h}_{i,j}(z_{k+i,j})\right\|^{2} \right) \\ \nonumber
&=\sum_{i=1}^{l}\left\| \tilde{\mathcal{H}}_{i} \left( \begin{array}{c}
\Delta X^{o} \\ \Delta X^{p}_{k+i}
\end{array} \right)  - \beta^{h}_{i} \right\|^{2}
\end{align}
where 
\begin{align}
\tilde{\mathcal{H}}_{i} &= \left( \begin{array}{c}
\left( \bar{\Omega}^{i,1}_{\xi} \right)^{1/2}h'_{1} \\
\left( \bar{\Omega}^{i,2}_{\xi} \right)^{1/2}h'_{2} \\
\vdots \\
\left( \bar{\Omega}^{i,n_{1}}_{\xi} \right)^{1/2}h'_{n_{1}} 
\end{array} \right)_{2 n_{1} \times 6} \\
\beta^{h}_{i} &= \left( \begin{array}{c}
\left( \bar{\Omega}^{i,1}_{\xi} \right)^{1/2}b^{h}_{i,1}(z_{k+i, 1}) \\
\left( \bar{\Omega}^{i,2}_{\xi} \right)^{1/2}b^{h}_{i,2}(z_{k+i, 2}) \\
\vdots \\
\left( \bar{\Omega}^{i,n_{1}}_{\xi} \right)^{1/2}b^{h}_{i,n_{1}}(z_{k+i, n_{1}}) 
\end{array} \right)_{2 n_{1} \times 1}
\end{align}
Since the state vector $ \Delta X_{k+i} $ can be separated into $ \Delta X^{o} $ and $ \Delta X^{p}_{i+1} $, here we also partition the $ \tilde{\mathcal{H}}_{i} $ into two parts
\begin{equation}
\tilde{\mathcal{H}}_{i} = \left( \begin{array}{cccc|cccc}
\tilde{\mathcal{H}}_{1\Delta} & \undermat{3 k}{0 & \cdots & 0} & \undermat{3 l}{ \tilde{\mathcal{H}}_{1\nabla} & 0 & \cdots & 0}
\end{array} \right)
\end{equation}
Now we stack $ \tilde{\mathcal{H}}_{i} $ and get $ \tilde{\mathcal{H}} $
\begin{equation}
\tilde{\mathcal{H}} = \left( \begin{array}{cccc|cccc}
\tilde{\mathcal{H}}_{1\Delta} & 0 & \cdots & 0 & \tilde{\mathcal{H}}_{1\nabla} & 0 & \cdots & 0\\
\tilde{\mathcal{H}}_{2\Delta} & 0 & \cdots & 0 & 0 & \tilde{\mathcal{H}}_{2\nabla} & \cdots & 0\\
\vdots & \vdots & \vdots & \vdots & \vdots & \vdots & \ddots & 0\\
\tilde{\mathcal{H}}_{l\Delta} & \undermat{3 k}{0 & \cdots & 0} & \undermat{3 l}{ 0 & 0 & \cdots & \tilde{\mathcal{H}}_{l\nabla}}
\end{array} \right)_{ 2 (\sum_{i=1}^{l}n_{i}) l \times 3(k+l+1) }
\label{eq::H}
\end{equation}
\newline
\begin{equation}
\beta^{h} = \left( \begin{array}{c}
\beta^{h}_{1} \\ \beta^{h}_{2} \\ \vdots \\ \beta^{h}_{l}
\end{array} \right)_{2\left( \sum_{i=1}^{l} n_{i}\right)l \times 1}
\label{eq::beta}
\end{equation}

Finally, we are able to stack $ \mathcal{G} $ and $ \tilde{\mathcal{H}} $ together into quadratic formulation
\begin{equation}
\left\| \mathcal{A}\left( u_{k:k+l-1} \right)\Delta X_{k+l} - \bar{b}_{k+l}\left( u_{k:k+l-1}, Z_{k+1:k+l} \right) \right\|^{2}
\end{equation}
where 
\begin{align}
\mathcal{A}_{k+l} &= \left( \begin{array}{c}
\left( \begin{array}{cc}
\Sigma^{1/2}_{k} & \mathbf{0}
\end{array} \right)\\
\mathcal{G} \\
\tilde{\mathcal{H}} 
\end{array} \right)_{\left( 3(k+1)+3l+2\left( \sum_{i=1}^{l} n_{i} \right)l \right)\times 3\left( k+l+1 \right)}, \\
\bar{b}_{k+l} &= \left( \begin{array}{c}
\mathbf{0} \\ \mathbf{0} \\ \beta^{h}
\end{array} \right)_{\left( 3(k+1)+3(l-1)+2\left( \sum_{i=1}^{l} n_{i} \right)l \right)\times 1}
\end{align}
The posterior covariance is updated as
\begin{equation}
\Sigma_{k+l}\left( u_{k:k+l-1} \right)|_{3(k+l+1)\times 3(k+l+1)} \doteq \mathcal{A}^{\intercal}_{k+l}\mathcal{A}_{k+l}
\label{eq::posterior_cov}
\end{equation}
and the mean of the general belief is
\begin{equation}
X^{\Delta}_{k+l} = \bar{X}_{k+l} + \left( \mathcal{A}^{\intercal}_{k+l} \mathcal{A}_{k+l} \right)^{-1}\mathcal{A}^{\intercal}_{k+l}\bar{b}_{k+l}
\label{eq::posterior_mean}
\end{equation}

\section*{Formulating Objective Function}

In our problem, there are two factors needs to be taken into consideration:

\begin{enumerate}
	\item Uncertainty in pose estimation and observation
	\item Observed number of features/repeatability
\end{enumerate}

\subsection*{Uncertainty}

The uncertainty is modelled as below using the marginal covariance of robot pose estimation
\begin{equation}
J^{u}_{k}\left( u_{k:k+L-1} \right) = \sum_{l = 0}^{L} tr\left( M_{\Sigma} \Sigma^{-1}_{k+l} M^{\intercal}_{\Sigma} \right)
\end{equation}

\subsection*{Observation}
The object recognition probability/score is computed as the equation below
\begin{equation}
J^{o}_{k}\left( u_{k:k+L-1} \right) = \mathbb{E} \left( \sum_{i=1}^{n_{k}}\left( 1- (1-s_{i})^{\tau_{j}} \right) \right) 
\end{equation}
where $ n_{k} $ is the number of observable features across $ L $ steps, $ s_{i} $ is object classification probability given feature $ i $ observed, $ \tau_{j} $ is the times where feature $ i $ is observed during the planning phase and also a function of control $ u_{k:k+L-1} $.

The weight across two factors can be manipulated by $ M_{\Sigma} $ and the overall objective function is expressed as
\begin{equation}
J_{k}(u_{k:K+L-1}) = \sum_{l = 0}^{L} tr\left( M_{\Sigma} \Sigma^{-1}_{k+l} M^{\intercal}_{\Sigma} \right) + \mathbb{E} \left( \sum_{i=1}^{n_{k}}\left( 1- (1-s_{i})^{\tau_{j}} \right) \right) 
\end{equation}

Now we try to rewrite the $ 2 $nd in above equation. Assume $ \Upsilon |_{n \times L}$ as the observability matrix where $ n $ is the number of features on the object and $ L $ is the steps number. $ \Upsilon |_{n \times L} $ is a matrix consisted by $ s_{i} $ and it is rewrite as
\begin{equation}
\mathbb{E} \left( \sum_{i=1}^{n_{k}}\left( 1- (1-s_{i})^{\tau_{j}} \right) \right) = l\left( \mathbb{E}\left( \Upsilon |_{n \times L} \right) \right)
\end{equation}

\begin{equation}
J_{k}(u_{k:K+L-1}) = \sum_{l = 0}^{L} tr\left( M_{\Sigma} \Sigma^{-1}_{k+l} M^{\intercal}_{\Sigma} \right) + l\left( \mathbb{E}\left( \Upsilon |_{n \times L} \right) \right)
\label{eq::obj_func}
\end{equation}

The $ 2 $nd term is a function of the expectation of $\Upsilon$. $ \Upsilon $ is a function of $ \mathbf{X}^{\Delta}_{k+L} $ and further a function of control input $ u_{k:k+L-1} $. Here we need to compute the representation of $ \Upsilon = \mathbb{K} \left( \mathbf{X}^{\Delta}_{k+L} \right) $ explicitly. 

First of all, given a feature position w.r.t. the object coordinate system $ f_{j}|_{o} = \left[ x^{f}_{j}|_{o}, y^{f}_{j}|_{o} \right] $, the object estimated pose $ X^{o} $ and robot pose $ X^{p} $, we are able to compute the $ s_{j} = k_{j}\left( X^{o}, X^{p} \right) $. Also, the estimated $ \mathbf{X}^{\Delta}_{k} $ won't be able to provide additional information on the newly observed features and estimated $ X_{k+L} $ includes the updated state estimation from step $ k+1 $ to step $ k+L-1 $ , the $ \mathbb{K}(\mathbf{X}^{\Delta}_{k+L-1}) $ is simplified into $ \mathbb{K}(X^{\Delta}_{k+L}) $ where
\begin{equation}
X^{\Delta}_{k+L} = \bar{X}_{k+L} + \left( \mathcal{A}^{\intercal}_{k+L}\mathcal{A}_{k+L} \right)^{-1}\mathcal{A}^{\intercal}_{k+l}\bar{b}_{k+L}
\end{equation}
Let's take a detail look at $ \mathbb{K}\left( X^{\Delta}_{k+L} \right) $. 
\begin{equation}
X^{\Delta}_{k+L} = \left( \begin{array}{c}
X^{o} \\ X^{p}_{1} \\ \vdots \\ X^{p}_{k} \\ X^{p}_{k+1} \\ \vdots \\ X^{p}_{k+L}
\end{array} \right)
\end{equation}
The newly observed feature only related with $ \left( X^{o}, X^{p}_{k+1}, \cdots, X^{p}_{k+L} \right) $.

Now we write $ \Upsilon $ using function $ k $
\begin{equation}
\Upsilon = \left( \begin{array}{cccc}
k_{1}\left(X^{o}, X^{p}_{k+1}\right) & k_{1}\left(X^{o}, X^{p}_{k+2}\right) & \cdots & k_{1}\left(X^{o}, X^{p}_{k+L}\right) \\ 
k_{2}\left(X^{o}, X^{p}_{k+1}\right) & k_{2}\left(X^{o}, X^{p}_{k+2}\right) & \cdots & k_{2}\left(X^{o}, X^{p}_{k+L}\right) \\ 
\vdots & \vdots & \ddots & \vdots \\
k_{n}\left(X^{o}, X^{p}_{k+1}\right) & k_{n}\left(X^{o}, X^{p}_{k+2}\right) & \cdots & k_{n}\left(X^{o}, X^{p}_{k+L}\right) \\ 
\end{array} \right)_{n\times L}
\end{equation}


\section*{Optimisation}

\subsection*{Discussion}
\textbf{Problem}: The uncertainty of the features due to the uncertainty of the object pose needs to be taken into consideration.

\textbf{Solved}: Assume the covariance of the object pose is $ \Sigma_{o} $ (here the state vector only contains the object pose). Given the coordinate of feature $ f $ on the object $\left( x^{f}|_{o}, y^{f}|_{o} \right) $, we can compute the uncertainty of the feature in world coordinate frame:

\begin{align}
\Sigma^{f}|_{o} &= J_{f} \Sigma_{o} J^{\intercal}_{f} \\
&= 
\left( \begin{array}{ccc}
1 & 0 & y^{f}|_{o} cos \theta^{o} - x^{f}|_{o} cos \theta^{o} \\
0 & 1 & -x^{f}|_{o} cos \theta^{o} - y^{f}|_{o} sin \theta^{o} 
\end{array} \right) \Sigma_{o} 
\left( \begin{array}{cc}
1 & 0 \\
0 & 1 \\
y^{f}|_{o} cos \theta^{o} - x^{f}|_{o} cos \theta^{o}  & -x^{f}|_{o} cos \theta^{o} - y^{f}|_{o} sin \theta^{o} 
\end{array} \right) \\
&= 
\left( \begin{array}{cc}
\sigma_{o}^{xx} & \sigma_{o}^{xy} \\
\sigma_{o}^{yx} & \sigma_{o}^{yy}
\end{array} \right)
\end{align}
where $ J_{f} $ is the Jacobian matrix w.r.t. $\left( x^{f}|_{o}, y^{f}|_{o} \right) $. And the covariance function of feature $ f $ is 
\begin{align}
\sigma_{o}^{xx} = \sigma^{xx}_{o} + ()
\end{align}


\begin{appendices}
\section{Linearisation in Optimisation}
\begin{align}
X^{\Delta}_{k+l} 
&= \underset{X_{k+l}}{\arg\min}||X_{k}-X^{\Delta}_{k}||^{2}_{\Sigma_{k}} && \\ \nonumber 
&+ \sum_{i=1}^{l}||X_{k+i}-\mathbf{G}(X_{k+i-1}, u_{k+i-1})||^{2}_{\Omega_{\eta}} \\  \nonumber
&+ \sum_{i=1}^{l}\sum_{j=1}^{n_{i}}||z_{k+i,j}-h(X_{k+i},f_{j}|_{o})||^{2}_{\bar{\Omega}_{\xi}} \\ \nonumber
&= \underset{X_{k+l}}{\arg\min}\left\| \Delta X_{k} \right\|^{2}_{\Sigma_{k}} \\ \nonumber
&+ \sum_{i=1}^{l}\left\| X_{k+i} - \bar{X}_{k+i} + \bar{X}_{k+i} -\mathbf{G}(X_{k+i-1}, u_{k+i-1}) \right\|^{2}_{\Omega_{\eta}} \\ \nonumber
&+ \sum_{i=1}^{l}\sum_{j=1}^{n_{i}}||z_{k+i,j}-h(\bar{X}_{k+i}, f_{j}|_{o})+h(\bar{X}_{k+i}, f_{j}|_{o})-h(X_{k+i},f_{j}|_{o})||^{2}_{\bar{\Omega}_{\xi}} \\ \nonumber
&= \underset{X_{k+l}}{\arg\min}\left\| \Delta X_{k} \right\|^{2}_{\Sigma_{k}} \\ \nonumber
&+ \sum_{i=1}^{l}\left\| \Delta X_{k+i}+ \bar{X}_{k+i} -\mathbf{G}(X_{k+i-1}, u_{k+i-1}) \right\|^{2}_{\Omega_{\eta}} \\ \nonumber
&+ \sum_{i=1}^{l}\sum_{j=1}^{n_{i}}||z_{k+i,j}-h(\bar{X}_{k+i}, f_{j}|_{o})+h'_{j} \Delta X_{k+i}||^{2}_{\bar{\Omega}_{\xi}} \\ \nonumber
\end{align}
where 
\begin{equation}
b^{h}_{i,j}\left( z_{k+i,j} \right) = z_{k+i,j}-h(\bar{X}_{k+i}, f_{j}|_{o})
\label{eq::rand_observation}
\end{equation}
Following by the update of the $ 2 $nd term
\begin{align}
X^{\Delta}_{k+l} 
&= \underset{X_{k+l}}{\arg\min}\left\| \Delta X_{k} \right\|^{2}_{\Sigma_{k}} \\ \nonumber
&+ \sum_{i=1}^{l}\left\| \Delta X_{k+i}+ \bar{X}_{k+i} - \mathbf{G}(\bar{X}_{k+i-1}, u_{k+i-1}) + \mathbf{G}(\bar{X}_{k+i-1}, u_{k+i-1}) -\mathbf{G}(X_{k+i-1}, u_{k+i-1}) \right\|^{2}_{\Omega_{\eta}} \\ \nonumber
&+ \sum_{i=1}^{l}\sum_{j=1}^{n_{i}}||h'_{j} \Delta X_{k+i} + b^{h}_{i,j}\left( z_{k+i,j} \right)||^{2}_{\bar{\Omega}_{\xi}} \\ \nonumber
&= \underset{X_{k+l}}{\arg\min}\left\| \Delta X_{k} \right\|^{2}_{\Sigma_{k}} \\ \nonumber
&+ \sum_{i=1}^{l}\left\| \Delta X_{k+i}+ \bar{X}_{k+i} - \mathbf{G}(\bar{X}_{k+i-1}, u_{k+i-1}) - \mathbf{G}'\Delta X_{k+i-1} \right\|^{2}_{\Omega_{\eta}} \\ \nonumber
&+ \sum_{i=1}^{l}\sum_{j=1}^{n_{i}}||h'_{j} \Delta X_{k+i} + b^{h}_{i,j}\left( z_{k+i,j} \right)||^{2}_{\bar{\Omega}_{\xi}} \\ \nonumber
&= \underset{X_{k+l}}{\arg\min}\left\| \Delta X_{k} \right\|^{2}_{\Sigma_{k}} \\ \nonumber
&+ \sum_{i=1}^{l}\left\| \Delta X_{k+i} - \mathbf{G}'\Delta X_{k+i-1} \right\|^{2}_{\Omega_{\eta}} \\ \nonumber
&+ \sum_{i=1}^{l}\sum_{j=1}^{n_{i}}||h'_{j} \Delta X_{k+i} + b^{h}_{i,j}\left( z_{k+i,j} \right)||^{2}_{\bar{\Omega}_{\xi}} \\ \nonumber
\end{align}

Now we need first to extract $ X^{o} $ and $ X^{p}_{i} $ from the eq.\ref{eq::pos_X}. By substituting eq.\ref{eq::G}, eq.\ref{eq::H} and eq.\ref{eq::beta}, we get 
\begin{equation}
\mathcal{A} = \left( \begin{array}{cccc|ccccc}
& & & & & & & & \\
\multicolumn{4}{c|}{\Sigma^{1/2}} & \multicolumn{5}{c}{\mathbf{0}} \\
& & & & & & & & \\
\hline \\ 
0 & \cdots & \cdots & -\Omega^{1/2}_{\eta}\mathbf{G}_{1} & \Omega^{1/2}_{\eta} & 0 & \cdots & 0 & 0 \\
0 & \cdots & \cdots & 0 & -\Omega^{1/2}_{\eta}\mathbf{G}_{2} & \Omega^{1/2}_{\eta} & \cdots & 0 & 0 \\
\vdots & \vdots & \vdots & \vdots & 0 & \ddots & \ddots & 0 & 0 \\
\vdots & \vdots & \vdots & \vdots & 0 & 0 & \ddots & \ddots & 0 \\
\vdots & \vdots & \vdots & \vdots & 0 & 0 & 0 & -\Omega^{1/2}_{\eta}\mathbf{G}_{L-1} & \Omega^{1/2}_{\eta} \\
\hline \\
\tilde{\mathcal{H}}_{1\Delta} & 0 & \cdots & 0 & \tilde{\mathcal{H}}_{1\nabla} & 0 & \cdots & \cdots & 0 \\
\tilde{\mathcal{H}}_{2\Delta} & 0 & \cdots & 0 & 0 & \tilde{\mathcal{H}}_{2\nabla} & \cdots & \cdots & 0 \\
\vdots & \vdots & \vdots & \vdots & 0 & 0 & \ddots &  & 0 \\
\vdots & \vdots & \vdots & \vdots & 0 & 0 &  & \ddots & 0 \\
\tilde{\mathcal{H}}_{L\Delta} & 0 & \cdots & 0 & 0 & 0 & \cdots & \cdots & \tilde{\mathcal{H}}_{L\nabla} \\
\end{array} \right)
\end{equation}

\begin{equation}
\mathcal{A}^{\intercal}\mathcal{A} = \left( \begin{array}{ccccc}
\end{array} \right)
\end{equation}

	
\end{appendices}


\end{document}

