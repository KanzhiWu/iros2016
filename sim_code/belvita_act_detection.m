% active object detection
% belvita boxes

clear all;
close all;
clc;

addpath('./utils/');
addpath('./mutils/');
addpath('./models/belvita/');

global conf
run belvita_conf_script;

f = figure;
for i = 1:length(conf.objs)
    % compute distance from objs to the robot
    dist = norm(conf.robPose(1:2)-conf.objs{i}.Pose(1:2));
    sigScale = (dist/0.6)*(dist/0.6)*(dist/0.6); % scale of sigma
    sig = sigScale.*conf.objSigma;
    Xts(i, :) = [conf.objs{i}.Pose(1)+rand()*sig(1), ...
                 conf.objs{i}.Pose(2)+rand()*sig(2), ...
                 conf.objs{i}.Pose(3)+rand()*sig(3)];
    XtCovs(i, :, :) = [sig(1)^2, 0, 0; 0, sig(2)^2, 0; 0, 0, sig(3)^2];
end
belvita_update_figure(f, Xts, XtCovs, conf.robPose, 'k');

% control constraint, enable both direction V
uub = kron([conf.V, conf.W], ones(conf.nSteps, 1));
ulb = kron([-conf.V, -conf.W], ones(conf.nSteps, 1));
uub = 1.01.*uub;
ulb = 1.01.*ulb;

Rt = conf.robPose;
confirmed = 0;
iter = 1;


while ~confirmed
    % greedy search for initialisation
    fprintf('Compute initialisation path for step %d.\n', iter);
    tic;
    [ctrlX0, ctrlF0] = belvita_greedy_sampling(Rt, Xts, XtCovs, f);
    initTime = toc;
    fprintf('Find the initilisation path with %f seconds.\n', initTime);
    
end
