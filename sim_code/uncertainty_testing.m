% uncertainty testing

clear all;
close all;
clc;

addpath('./utils/');
disp('Load configuration data and visualisation ...');
global conf
run conf_script;
%{
for i = 1:1
    % generate random points
    ranAngle = -rand()*pi/10; %rand()*2*pi;
    conf.feats(conf.featsn+i, :) = [conf.objRad*cos(ranAngle) conf.objRad*sin(ranAngle)];
    conf.featsW(end+i) = 1.0;
end
conf.featsW = conf.featsW/size(conf.feats, 1);
conf.featsn = size(conf.feats, 1);
%}
f = figure;

% init pose 
Xt = conf.objPose;
%Xt = [rand()*conf.objSigmaX rand()*conf.objSigmaY rand()*conf.objSigmaO];
XtCov = [conf.objSigmaX^2, 0, 0; 0, conf.objSigmaY^2, 0; 0, 0, conf.objSigmaO^2];
trace(XtCov)
% robot poses
Rt = [0, -0.8, 0];
ctrl1 = [1, 5*asin(0.125); 1, 10*asin(0.125); 1, 10*asin(0.125); 1, 10*asin(0.125); 1, 10*asin(0.125)];
ctrl2 = [0.1387    0.1499
    0.1642    0.0200
    0.1785    0.1899
    0.1734    0.1196
    0.1184    0.3680];
    %1.0000    3.1416
    %1.0000         0
    %1.0000         0
    %1.0000         0
    %1.0000    3.1416
%ctrl2 = [0.3523, 0.0239; 0.3780, 2.5960; 0.3892, 0.0060; 0.3820, -2.5861; 0.3783, 0.0006];
%paths1 = Rt;
Rt1 = Rt;
for i = 1:size(ctrl1)
    Rt1 = motion(Rt1, ctrl1(i, :));
    paths1(i, :) = Rt1;
end

%paths2 = Rt;
Rt2 = Rt;
for i = 1:size(ctrl2)
    Rt2 = motion(Rt2, ctrl2(i, :));
    paths2(i, :) = Rt2;
end


%paths1 = [0, -0.8, 0; 0.8, 0, pi/2; 0, 0.8, pi; -0.8, 0, 3*pi/2];%; 0, -0.8, 0];
%paths2 = [0, -0.8, 0; 0, -0.8, 0; 0, -0.8, 0; 0, -0.8, 0];

update_figure(f, Xt, XtCov, paths1, 'k');
update_figure(f, Xt, XtCov, paths2, 'r');

sOmegaObj = sqrt(inv(conf.R));

oFeats = conf.feats;
oFeats(:, 3) = 1;
rtmat = [ cos(Xt(3)) -sin(Xt(3)) Xt(1); ...
          sin(Xt(3)) cos(Xt(3)) Xt(2); ...
          0 0 1];
tFeats = (rtmat*(oFeats)')';

H1(1:3, :) = sqrt(inv(XtCov));
H2(1:3, :) = sqrt(inv(XtCov));
find = 4;
S1 = zeros(conf.featsn, 2);

for i = 1:size(paths1, 1)
    rbpt = paths1(i, :);
    for j = 1:size(conf.feats, 1)
        xfo = conf.feats(j, 1); yfo = conf.feats(j, 2);
        p1 = rbpt; p2 = tFeats(j, :);
        thp = rbpt(3); tho = Xt(3);
        if ~intersection_pts(p1(1:2), p2(1:2), Xt(1:2), conf.objRad)
            % check FOV
            n1 = [cos(rbpt(3)+pi/2), sin(rbpt(3)+pi/2)];
            % compute ori from robot to feature
            n2 = [p2(1)-p1(1), p2(2)-p1(2)];
            ang = acos(dot(n1,n2)/(norm(n2)));
            if ang < conf.FOV/2;
                nf = [p2(1)-Xt(1), p2(2)-Xt(2)];
                drc = -n2;
                angf = acos(dot(nf, drc)/(norm(nf)*norm(drc)));
                dstf = norm(drc);
                obsVal = feature_rep(angf, dstf);
                S1(j, i) = obsVal;

                hjaco = [cos(thp), sin(thp), xfo*(cos(tho)*sin(thp) - cos(thp)*sin(tho)) - yfo*(cos(tho)*cos(thp) + sin(tho)*sin(thp));
                         -sin(thp), cos(thp), xfo*(cos(tho)*cos(thp) + sin(tho)*sin(thp)) + yfo*(cos(tho)*sin(thp) - cos(thp)*sin(tho))];
                omega = obsVal.*sOmegaObj;
                hjaco = omega*hjaco;
                H1(find:find+1,:) = hjaco;        
                find = find+2;
            end
        end  
    end
end
s1 = trace(inv(H1'*H1))



S2 = zeros(conf.featsn, 2);
find = 4;
for i = 1:size(paths2, 1)
    rbpt = paths2(i, :);
    for j = 1:size(conf.feats, 1)
        xfo = conf.feats(j, 1); yfo = conf.feats(j, 2);
        p1 = rbpt; p2 = tFeats(j, :);
        thp = rbpt(3); tho = Xt(3);
        if ~intersection_pts(p1(1:2), p2(1:2), Xt(1:2), conf.objRad)
            % check FOV
            n1 = [cos(rbpt(3)+pi/2), sin(rbpt(3)+pi/2)];
            % compute ori from robot to feature
            n2 = [p2(1)-p1(1), p2(2)-p1(2)];
            ang = acos(dot(n1,n2)/(norm(n2)));
            if ang < conf.FOV/2;
                nf = [p2(1)-Xt(1), p2(2)-Xt(2)];
                drc = -n2;
                angf = acos(dot(nf, drc)/(norm(nf)*norm(drc)));
                dstf = norm(drc);
                %fprintf('angle = %f, distance = %f\n', angf, dstf);
                obsVal = feature_rep(angf, dstf);
                S2(j, i) = obsVal;
                hjaco = [cos(thp), sin(thp), xfo*(cos(tho)*sin(thp) - cos(thp)*sin(tho)) - yfo*(cos(tho)*cos(thp) + sin(tho)*sin(thp));
                         -sin(thp), cos(thp), xfo*(cos(tho)*cos(thp) + sin(tho)*sin(thp)) + yfo*(cos(tho)*sin(thp) - cos(thp)*sin(tho))];
                omega = obsVal.*sOmegaObj;
                hjaco = omega*hjaco;
                H2(find:find+1,:) = hjaco;        
                find = find+2;
            end
        end  
    end
    %disp('-------------------------------------------');
end
s2 = trace(inv(H2'*H2))
