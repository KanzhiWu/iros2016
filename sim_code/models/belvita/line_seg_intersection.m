function occl = line_seg_intersection( p1, p2, q1, q2 )
% p1    - start point of line p, robot
% p2    - start point of line p, feature
% q1    - start point of line q
% q2    - start point of line q

p = p1;
r = p2-p1;
q = q1;
s = q2-q1;



t = wu_cross(q-p, s)/wu_cross(r, s);
u = wu_cross(q-p, r)/wu_cross(r, s);
if wu_cross(r, s) == 0 
    if wu_cross(q-p, r) == 0
        % two line segment are colinear
        % and overlapping in some parts
        occl = 1;
    else
        % two line segment are parallel and non-intersecting
        occl = 0;
    end
else
    if u >= 0 && u <= 1.00 && t >= 0 && t <= 0.99
        %if t-1 < 0.001
        %    occl = 1;
        %else
        %    occl = 0;
        %end
        %fprintf('u = %f, t = %f\n', u, t);
        occl = 1;
    else
        occl = 0;
    end
end
            
    