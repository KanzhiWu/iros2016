function [ctrlX0, ctrlF0] = belvita_greedy_sampling(rbps, obps, obpCovs, f)
% find the optimal init path given current robot pose
% rbps   - current RoBot Pose
% obps   - estimated OBject Pose
% obpCovs- estimated object pose covariance
% f      - figure

global conf

rbp = rbps(end, :);
allpathsIdx = permn([1:length(conf.uAngles)*length(conf.uDists)], conf.nSteps);
% scores
xScores = zeros(size(allpathsIdx, 1), 1); % state estimation
fScores = zeros(size(allpathsIdx, 1), 1); % object recognition

tFeatsAll = {};
for i = 1:length(conf.objs)
    oFeats = conf.objs{i}.Feats;
    oFeats(:, 3) = 1;
    obp = obps(i, :);
    rtmat = [ cos(obp(3)) -sin(obp(3)) obp(1); ...
              sin(obp(3)) cos(obp(3)) obp(2); ...
              0 0 1];
    tFeatsAll{i} = (rtmat*(oFeats)')';
end

sOmegaObj = sqrt(inv(conf.R));

for i = 1:size(allpathsIdx, 1)
    % for two directions
    for dir = -1:2:1
        %H = zeros(3+conf.nSteps*conf.featsn*2, 3);
        for j = 1:length(conf.objs)
            H(3*(j-1)+1:3*(j-1)+3, :) = sqrt(inv(squeeze(obpCovs(j, :, :))));
        end
        find = 3*length(conf.objs)+1;
        rbpt = rbp;
        for j = 1:conf.nSteps
            u = idx2control(allpathsIdx(i, j));
            u(1) = conf.V*u(1)*dir;
            u(2) = conf.W*u(2);
            rbpt = motion(rbpt, u);
            %{
            figure;
            axis([conf.mapWidthR(1) conf.mapWidthR(2) conf.mapHeightR(1) conf.mapHeightR(2)])
            set(gca,'xtick',[conf.mapWidthR(1):conf.mapStep:conf.mapWidthR(2)]);
            set(gca,'ytick',[conf.mapHeightR(1):conf.mapStep:conf.mapHeightR(2)]);
            axis square
            %}
            
            %{
            poseTri = get_p_tri(rbpt(1:2), rbpt(3));
            hold on;
            patch(poseTri(:, 1), poseTri(:, 2), 'g');
            %}
            colli = belvita_collision_check(rbpt, obps);
            if colli; break; end;
            for k = 1:length(conf.objs)
                obp = obps(k, :);
                for jj = 1:size(conf.objs{k}.Feats, 1);
                    xfo = conf.objs{k}.Feats(jj, 1); 
                    yfo = conf.objs{k}.Feats(jj, 2);
                    p1 = rbpt; p2 = tFeatsAll{k}(jj, :);
                    %hold on;
                    %plot(p2(1), p2(2), 'g+');
                    thp = p1(3); tho = obp(3);
                    if ~belvita_intersection_pts(p1(1:2), p2(1:2), obps)                            
                        n1 = [cos(rbpt(3)+pi/2), sin(rbpt(3)+pi/2)];
                        n2 = [p2(1)-p1(1), p2(2)-p1(2)];
                        ang = acos(dot(n1,n2)/(norm(n2)*norm(n1)));
                        if ang < conf.FOV/2
                            %hold on;
                            %plot(p2(1), p2(2), 'g+', 'MarkerSize', 12);
                            nfo = conf.objs{k}.Normals(jj, :);
                            nf = [nfo(1)*cos(obp(3))-nfo(2)*sin(obp(3)), nfo(1)*sin(obp(3))+nfo(2)*cos(obp(3))];
                            drc = -n2;
                            angf = acos(dot(nf, drc)/(norm(nf)*norm(drc)));
                            %if angf <= pi/4
                            %    hold on;
                            %    plot(p2(1), p2(2), 'g+', 'MarkerSize', 12);                                
                            %end
                            dstf = norm(drc);
                            obsVal = feature_rep(angf, dstf);
                            S{k}(jj, j) = obsVal;
                            hjaco = [cos(thp), sin(thp), xfo*(cos(tho)*sin(thp) - cos(thp)*sin(tho)) - yfo*(cos(tho)*cos(thp) + sin(tho)*sin(thp));
                                     -sin(thp), cos(thp), xfo*(cos(tho)*cos(thp) + sin(tho)*sin(thp)) + yfo*(cos(tho)*sin(thp) - cos(thp)*sin(tho))];
                            omega = obsVal.*sOmegaObj;
                            hjaco = omega*hjaco;
                            H(find:find+1,:) = hjaco;            
                            find = find+2;
                        end
                    end
                end
            end
        end

        xScores(i) = trace(inv(H'*H));

        if ~colli
            if ~isempty(S)
                for j = 1:length(conf.objs)
                    fScores(i) = fScores(i)+get_obs_score(S{j});
                end
            else
                fScores(i) = 0;
            end
        end
        S = {};
    end
    if mod(i, 50) == 1
        disp(['Current iteration is : ' num2str(i)]);
    end
end

% find the best path w.r.t. x
[xmin, xind] = min(xScores);
% compute the path w.r.t. xind
xPoses = zeros(conf.nSteps+1, 3);
xPoses(1, :) = rbp;
xRobCurPose = rbp;
ctrlX0 = zeros(conf.nSteps, 2);

for j = 1:conf.nSteps
    u = idx2control(allpathsIdx(xind, j));
    u(1) = conf.V*u(1);
    u(2) = conf.W*u(2);
    ctrlX0(j, :) = u;
    xRobCurPose = motion(xRobCurPose, u);
    xPoses(j+1, :) = xRobCurPose;
end
% visualise initial path
multi_update_figure(f, obps, obpCovs, xPoses, 'g--');

[fmax, find] = max(fScores);
fPoses = zeros(conf.nSteps+1, 3);
fPoses(1, :) = rbp;
fRobCurPose = rbp;
ctrlF0 = zeros(conf.nSteps, 2);

for j = 1:conf.nSteps
    u = idx2control(allpathsIdx(find, j));
    u(1) = conf.V*u(1);
    u(2) = conf.W*u(2);
    ctrlF0(j, :) = u;
    fRobCurPose = motion(fRobCurPose, u);
    fPoses(j+1, :) = fRobCurPose;
end
% control_visualisation(f, obp, obpCov. rbp, ctrlF0, 'green');
% visualise initial path
multi_update_figure(f, obps, obpCovs, fPoses, 'r--');
