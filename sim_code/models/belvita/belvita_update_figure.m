function h = belvita_update_figure(f, xs, covs, poses, robColor)
% update belvita visualisation image
% f     - figure
% xs    - estimated object pose
% covs  - covariance robot pose
% poses - robot poses
% robColor - color


global conf

% setup display properties
%xlabel('metres'), ylabel('metres');
%set(f, 'name', 'Active Object Detection Simulator');
axis([conf.mapWidthR(1) conf.mapWidthR(2) conf.mapHeightR(1) conf.mapHeightR(2)])
%set(gca,'xtick',[conf.mapWidthR(1):conf.mapStep:conf.mapWidthR(2)]);
%set(gca,'ytick',[conf.mapHeightR(1):conf.mapStep:conf.mapHeightR(2)]);
axis square

for i = 1:length(conf.objs)
    % draw object ground truth pose
    tX = conf.objs{i}.Pose;
    tCorners = [-conf.objs{i}.Size(1)/2, conf.objs{i}.Size(1)/2, ...
                conf.objs{i}.Size(1)/2, -conf.objs{i}.Size(1)/2, -conf.objs{i}.Size(1)/2; ...
                -conf.objs{i}.Size(2)/2, -conf.objs{i}.Size(2)/2, ...
                conf.objs{i}.Size(2)/2, conf.objs{i}.Size(2)/2, -conf.objs{i}.Size(2)/2];
    tCorners = [tCorners; 1, 1, 1, 1, 1];
    tPose = conf.objs{i}.Pose;
    tRot = [cos(tPose(3)), -sin(tPose(3)), tPose(1); ...
            sin(tPose(3)), cos(tPose(3)), tPose(2); ...
            0, 0, 1];
    transCorners = tRot*tCorners;
    hold on;
    plot(transCorners(1,:), transCorners(2,:), 'k');
    
    % draw estimated object pose
    x = xs(i, :);
    cov = squeeze(covs(i, :, :));
    xRot = [cos(x(3)), -sin(x(3)), x(1); sin(x(3)), cos(x(3)), x(2); 0, 0, 1];
    xFeats = conf.objs{i}.Feats;
    xFeats = [xFeats, ones(size(xFeats, 1), 1)];
    xTransFeats = (xRot*xFeats')';
    covN = 50;
    covInc = 2*pi/covN;
    covPhi = 0:covInc:2*pi;
    
    for j = 1:conf.objs{i}.NFeats
        %hold on;
        %plot(xTransFeats(j, 1), xTransFeats(j, 2), 'r+');
        fo = xFeats(j, :)';
        fw = xTransFeats(j, :)';
        F = [1, 0, -fo(2)*cos(x(3))-fo(1)*sin(x(3)); ...
             0, 1, fo(1)*cos(x(3))-fo(2)*sin(x(3))];
        covF = F*cov*F';
        ellipF = make_ellipse(fw, covF, 0.1, covPhi);
        hold on;
        plot(ellipF(1,:), ellipF(2,:), 'r');
    end
    
end

% draw robot pose
for i = 1:size(poses, 1)
    poseTri = get_p_tri(poses(i, 1:2), poses(i, 3));
    hold on;
    patch(poseTri(:, 1), poseTri(:, 2), robColor);
end
if size(poses, 1) > 1
    for i = 2:size(poses, 1)
        stpt = poses(i-1, 1:2);
        enpt = poses(i, 1:2);
        hold on;
        plot([stpt(1) enpt(1)], [stpt(2) enpt(2)], robColor);
    end
end
