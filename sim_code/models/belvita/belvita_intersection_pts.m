function occl = belvita_intersection_pts(p1, p2, Xs)
% find occlusion status
% p1    - robot pose, start point
% p2    - end point, feature
% xs    - objects poses

global conf

occl = 0;
occlFound = 0;
for i = 1:length(conf.objs)
    w = conf.objs{i}.Size(1);
    h = conf.objs{i}.Size(2);
    vers = [-w/2, w/2, w/2, -w/2; -h/2, -h/2, h/2, h/2; 1, 1, 1, 1]; % vertices
    rot = [ cos(Xs(i, 3)), -sin(Xs(i, 3)), Xs(i, 1);
            sin(Xs(i, 3)), cos(Xs(i, 3)), Xs(i, 2);
            0, 0, 1];
    tVers = rot*vers;
    for j = 1:size(tVers, 2)
        j0 = j;
        j1 = mod(j, 4)+1;
        %g = figure;
        %hold on; plot([p1(1) p2(1)], [p1(2) p2(2)]);
        %hold on; plot([tVers(1, j0) tVers(1, j1)], [tVers(2, j0) tVers(2, j1)], 'r');
        if line_seg_intersection(p1, p2, tVers(1:2, j0)', tVers(1:2, j1)')
            occl = 1;
            occlFound = 1;
            break;
        end
    end
    if occlFound == 1
        break;
    end
end

