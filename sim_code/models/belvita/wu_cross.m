function r = wu_cross(v, w)
% compute the defined cross product for v and w
r = v(1)*w(2)-v(2)*w(1);

