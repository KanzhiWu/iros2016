function normals = rect_normals(feats, i)
% compute normals for rectangle
% feats     - features
% i         - object index
% normal    - normal vectors
global conf

w = conf.objs{i}.Size(1);
h = conf.objs{i}.Size(2);
normals = zeros(size(feats));
for i = 1:size(feats, 1)
    f = feats(i, :);
    if f(1) == w/2
        normals(i, :) = [1, 0];
    elseif f(1) == -w/2
        normals(i, :) = [-1, 0];
    elseif f(2) == h/2
        normals(i, :) = [0, 1];
    elseif f(2) == -h/2
        normals(i, :) = [0, -1];
    end
end
