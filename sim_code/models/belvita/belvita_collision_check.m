function colli = belvita_collision_check(rbp, Xs)
% check collision
% rbp   - robot pose
% Xs    - object poses


global conf
colli = 0;
% decide whether a point locate inside a rectangle
for i = 1:size(Xs, 1)
    w = size(conf.objs{i}.Size, 1);
    h = size(conf.objs{i}.Size, 2);
    vers = [-w/2, w/2, w/2, -w/2; -h/2, -h/2, h/2, h/2; 1, 1, 1, 1]; % vertices
    rot = [ cos(Xs(i, 3)), -sin(Xs(i, 3)), Xs(i, 1);
            sin(Xs(i, 3)), cos(Xs(i, 3)), Xs(i, 2);
            0, 0, 1];
    tVers = rot*vers;
    if check_in_rect(rbp(1:2), tVers(:,1)', tVers(:,2)', tVers(:,3)', tVers(:,4)');
        colli = 1;
        break;
    end
end
