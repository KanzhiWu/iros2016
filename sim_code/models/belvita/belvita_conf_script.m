% configuration script

% robot control model
conf.V = 1;   % upperbound for velocity m%s
conf.W = pi/2;    % upperbound for angular velocity rad%s
conf.DT = 0.2;  % time step, second

% robot control noise
conf.sigmaV = 0.2; 
conf.sigmaW = pi/6;
conf.Q = [conf.sigmaV^2 0; 0, conf.sigmaW^2];

% observation model
conf.maxRange = 2.0;
conf.minRange = 0.5;
conf.FOV = pi/3;

% observation noise
conf.sigmaX = 0.005;
conf.sigmaY = 0.005;
conf.sigmaZ = 0.005;
conf.R = [conf.sigmaX^2 0; 0 conf.sigmaZ^2];


% load circular object model
run RectObjModel

objNames = {'chocolate', 'fruit_fibre', 'honey_nut', 'milk_cereals'};
conf.nObj = 4;
% init object pose covariance
conf.objSigma = [0.01, 0.01, pi/10];

chocObj = RectObjModel;
chocObj.Label = 1;
chocObj.Name = objNames{1};
chocObj.Size = [0.1, 0.054];
chocObj.Pose = [-0.2, -0.2, rand()*2*pi];
load('chocolate.mat');
chocObj.Feats = feats(:, 1:2);
chocObj.NFeats = size(feats, 1);
chocObj.FeatsW = feats(:, 4);
chocObj.FeatsS = feats(:, 3);

fruitObj = RectObjModel;
fruitObj.Label = 2;
fruitObj.Name = objNames{2};
fruitObj.Size = [0.1, 0.054];
fruitObj.Pose = [0, 0, rand()*2*pi];
load('fruit_fibre.mat');
fruitObj.Feats = feats(:, 1:2);
fruitObj.NFeats = size(feats, 1);
fruitObj.FeatsW = feats(:, 4);
fruitObj.FeatsS = feats(:, 3);

honeyObj = RectObjModel;
honeyObj.Label = 2;
honeyObj.Name = objNames{2};
honeyObj.Size = [0.1, 0.054];
honeyObj.Pose = [-0.1, 0.1, rand()*2*pi];
load('honey_nut.mat');
honeyObj.Feats = feats(:, 1:2);
honeyObj.NFeats = size(feats, 1);
honeyObj.FeatsW = feats(:, 4);
honeyObj.FeatsS = feats(:, 3);

milkObj = RectObjModel;
milkObj.Label = 2;
milkObj.Name = objNames{2};
milkObj.Size = [0.1, 0.054];
milkObj.Pose = [0.1, -0.1, rand()*2*pi];
load('milk_cereals.mat');
milkObj.Feats = feats(:, 1:2);
milkObj.NFeats = size(feats, 1);
milkObj.FeatsW = feats(:, 4);
milkObj.FeatsS = feats(:, 3);

conf.objs{1} = chocObj;
conf.objs{2} = fruitObj;
conf.objs{3} = honeyObj;
conf.objs{4} = milkObj;

for i = 1:length(conf.objs)
    conf.objs{i}.Normals = rect_normals(conf.objs{i}.Feats, i);
end
    
% original robot start pose
conf.robPose = [0 -0.8 0];

% grid size
conf.gridSize = conf.V*conf.DT/2;
conf.gridSearch = 20;
conf.gridRadSize = conf.W*conf.DT;
conf.width = [-1, 1];
conf.height = [-1, 1];


% motion/path planning parameter
conf.nSteps = 5;
conf.uAngles = [-conf.W*conf.DT, 0, conf.W*conf.DT];
%conf.uDists = [-conf.V*conf.DT, conf.V*conf.DT];
conf.uDists = [conf.V*conf.DT];

% grid map setup
conf.mapWidthR = [-1 1];
conf.mapHeightR = [-1 1];
conf.mapStep = 0.4;

conf.gridSize = 0.01;
% build gridmap
conf.mapWidth = conf.mapWidthR(2)-conf.mapWidthR(1);
conf.mapHeight = conf.mapHeightR(2)-conf.mapHeightR(1);
conf.gridMap = zeros(conf.mapHeight/conf.gridSize, conf.mapWidth/conf.gridSize);
conf.gridMapWidth = conf.mapWidth/conf.gridSize;
conf.gridMapHeight = conf.mapHeight/conf.gridSize;


% estimator
conf.Xbb = 1.0;

