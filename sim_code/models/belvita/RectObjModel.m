% rectangular object model
% belvita box

classdef RectObjModel
    properties
        Label % int, 1, 2, ...
        Name  % object name
        Size  % width and height
        Pose  % centre
        Feats
        NFeats
        FeatsW
        FeatsS % feature scale
        Normals % normal vectors
    end
end