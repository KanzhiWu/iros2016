function in = check_in_rect(p, v1, v2, v3, v4)
% check the point is in a rectangle or not
% v1~v4     - vertices of the rectangle
% p         - point

v12 = v2-v1; v14 = v4-v1;
areaRect = norm(v12)*norm(v14);

% compute the sum of areas of 4 triangles

areaTris = 0.0;
areaTris = areaTris + get_triangle(p, v1, v2);
areaTris = areaTris + get_triangle(p, v2, v3);
areaTris = areaTris + get_triangle(p, v3, v4);
areaTris = areaTris + get_triangle(p, v4, v1);

if abs(areaTris-areaRect) < 0.001
    in = 1;
else
    in = 0;
end
    



