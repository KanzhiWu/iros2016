function p_tri = get_p_tri(pos, ori)
% triangle from robot pose
% - position: x and y
% - orientation: ori in radius
p_size = [0.05, 0.08]; % width of tri, height of tri
corners = [0, p_size(1)/2; 0, -p_size(1)/2; p_size(2), 0 ]';
ori = ori+pi/2;
rotmat = [ cos(ori), -sin(ori), pos(1); sin(ori), cos(ori), pos(2); 0, 0, 1 ];
for i = 1:size(corners, 2)
    orip = [corners(:, i); 1];
    glop = rotmat*orip;
    p_tri(i, 1) = glop(1);
    p_tri(i, 2) = glop(2);
end
%%