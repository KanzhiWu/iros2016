function S = get_obs_matrix(x, sigmax, poses)
% compute the observation matrix for all features
% x     - state of the object
% sigmax- object pose covariance
% poses - robot poses;

global conf

threshp = 0.0; % threshold for p, due to the sigmax
thresho = 0.0; % threshold for observation
threshfov = conf.FOV;
objRadius = conf.objRad;

sigmaOx = conf.sigmaX; sigmaOz = conf.sigmaZ;
omegaObj = conf.R;
somegaObj = sqrt(inv(omegaObj));

S = zeros(conf.featsn, size(poses, 1));
for i = 1:size(poses, 1)
    % pose 
    rbpt = poses(i, :);
    ss = zeros(1, conf.featsn);
    for j = 1:conf.featsn
        % compute feature uncertainty
        xf = conf.feats(j, 1); yf = conf.feats(j, 2);
        F = [1, 0, -yf*cos(x(3))-xf*sin(x(3)); ...
             0, 1, xf*cos(x(3))-yf*sin(x(3))];
        sigmaf = F*sigmax*F';
        pf = 1/((2*pi)^2*det(sigmaf));
        if pf > threshp
            % compute observability
            p1 = rbpt(1:2);
            p2 = [xf yf 1];
            rtmat = [ cos(x(3)) -sin(x(3)) x(1); ... 
                      sin(x(3)), cos(x(3)), x(2); ... 
                      0, 0, 1 ];
            p2 = (rtmat*p2')';
            if ~intersection_pts(p1, p2(1:2), x(1:2), objRadius)    % check occlusion
                % check FoV
                n1 = [cos(rbpt(3)+pi/2), sin(rbpt(3)+pi/2)];
                % compute ori from robot to feature
                n2 = [p2(1)-p1(1), p2(2)-p1(2)];
                ang = acos(dot(n1,n2)/(norm(n2)));
                if ang < threshfov/2;
                    % compute the score
                    nf = [p2(1)-x(1), p2(2)-x(2)];
                    drc = -n2;
                    angf = acos(dot(nf, drc)/(norm(nf)*norm(drc)));
                    dstf = norm(drc);
                    ss(j) = feature_rep(angf, dstf);
                end       
            end
        end
    end
    S(:, i) = ss';
end