%% Observation model
syms xfo yfo;
syms xo yo tho;
Gwo = [cos(tho) -sin(tho) xo; sin(tho) cos(tho) yo; 0 0 1];

syms xp yp thp;
Gpw = [cos(thp) sin(thp) -cos(thp)*xp+sin(thp)*yp; ...
       -sin(thp) cos(thp) sin(thp)*xp-cos(thp)*yp; ...
       0 0 1];
syms pp;
pp = Gpw*Gwo*[xfo; yfo; 1];
Hx = jacobian(pp(1),[xo, yo, tho]);
Hy = jacobian(pp(2),[xo, yo, tho]);


% 
