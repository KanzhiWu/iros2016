function p = feature_rep(ang, dist)
% compute feautre matching probability/score
% based on feature model
% ang  - angle between normal and vector from feature to pose
% dist - distance from feature to pose

if dist < 0.5
    p = 0;
elseif ang > 2*pi/3
    p = 0;
else
    m_dist = 0.6;
    s_dist = 0.2;
    s_ori = 2*pi*60/180;


    v_d = exp(-(dist-m_dist)^2/(2*s_dist^2));
    v_o = exp(-(ang)^2/(2*s_ori^2));
    p = v_d*v_o; 
end



