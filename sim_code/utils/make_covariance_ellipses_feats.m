function p = make_covariance_ellipses_feats(x, P)
% compute ellipses for feature state vector
N = 50;
inc = 2*pi/N;
phi = 0:inc:2*pi;
lenf = length(x)/2;
p = zeros(2, (lenf)*(N+2));
ctr = 1;
for i = 1:lenf
    ii = ctr:(ctr+N+1);
    jj = (i-1)*2+1; jj = jj:jj+1;
    p(:, ii) = make_ellipse(x(jj), P(jj, jj), 0.1, phi);
    ctr = ctr+N+2;
end
%%