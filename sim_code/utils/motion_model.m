%% Motion model

syms v w x y th
syms dt
%%
% S. Thrun, probabilistic robotics
xs = x + (-v/w*(sin(th)) + v/w*sin(th + w*dt));
ys = y + (-v/w*(sin(th)) + v/w*sin(th + w*dt));
ths = th + w*dt;
Gs = jacobian([xs ys ths], [x y th])
Gu = jacobian([xs ys ths], [v w])
  
%%
% G. Dissanayake
xg = x + v*dt*cos(th+w*dt);
yg = y + v*dt*sin(th+w*dt);
thg = th + w*dt;
Gg = jacobian([xg yg thg], [x y th])
Gu = jacobian([xg yg thg], [v w])



 
