% object model

classdef ObjModel
    properties
        Label
        Rad
        Pose
        PoseCov
        Feats
        NFeats
        FeatsW
    end
end