function colli = collision_check(rbp)
% check collision
% rbp   - robot pose

global conf

% grid map coordinate
gRbpX = ceil((rbp(1)-conf.mapWidthR(1))/conf.gridSize);
gRbpY = ceil((rbp(2)-conf.mapHeightR(1))/conf.gridSize);
if gRbpX <= 0 || gRbpX > conf.gridMapWidth || gRbpY <= 0 || gRbpY > conf.gridMapHeight
    colli = 0;
else 
    colli = conf.gridMap(gRbpY, gRbpX);
end
    