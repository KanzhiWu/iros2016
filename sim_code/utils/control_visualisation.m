function h = control_visualisation(f, x, sigmax, r, ctrls, robColor)
% visualise robot pose given controls
% f     - figure
% x     - object pose
% sigmax- object pose uncertainty
% r     - robot pose
% ctrls - control
% robColor - color


poses = zeros(size(ctrls, 1)+1, 3);
poses(1, :) = r;
rt = r;

for j = 1:size(ctrls, 1)
    rt = motion(rt, u);
    poses(j+1, :) = rt;
end

update_figure(f, x, sigmax, poses, robColor);