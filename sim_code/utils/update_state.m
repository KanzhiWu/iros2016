function [Xt1, XtCov1] = update_state( Xt, XtCov, tXt, Rt)
% update state after observing new observation
% Xt1   - state of t+1
% XtCov1- uncertainty of state t+1
% Xt    - state of time t
% XtCov - uncertainty of state time t
% tXt   - ground truth object pose
% Rt    - robot pose



global conf

% compute jacobian
% H = zeros(2*conf.featsn, 3);

% compute error between estimated observation and actual observation
z = obser_feature(Xt, Rt);
zT = obser_feature(tXt, Rt, 0); % add noise to accurate prediction

ind = 1;
xp = Rt(1); yp = Rt(2); thp = Rt(3);
xo = Xt(1); yo = Xt(2); tho = Xt(3);

for i = 1:conf.featsn
    if zT(i, 1) ~= 0 && zT(i, 2) ~= 0 && z(i, 1) ~= 0 && z(i, 2) ~= 0
        xfo = conf.feats(i, 1); yfo = conf.feats(i, 2);
        H(ind, 1) = cos(thp);
        H(ind, 2) = sin(thp);
        H(ind, 3) = xfo*(cos(tho)*sin(thp) - cos(thp)*sin(tho)) + yfo*(cos(tho)*cos(thp) + sin(tho)*sin(thp));
        H(ind+1, 1) = -sin(thp);
        H(ind+1, 2) = cos(thp);
        H(ind+1, 3) = xfo*(cos(tho)*cos(thp) + sin(tho)*sin(thp)) + yfo*(cos(tho)*sin(thp) - cos(thp)*sin(tho));
        ind = ind+2;
    end
end

Q = kron(eye(size(H, 1)/2), conf.R);
K = XtCov*H'*inv(H*XtCov*H'+Q);

z = reshape(z', numel(z), 1);
zT = reshape(zT', numel(zT), 1);

ind = 1;
for i = 1:size(z, 1)
    if  zT(i) ~= 0 && z(i) ~= 0%
        zDiff(ind) = zT(i)-z(i);
        ind = ind+1;
    end
end
Xt1 = (Xt'+K*zDiff')';
XtCov1 = (eye(3)-K*H)*XtCov;
