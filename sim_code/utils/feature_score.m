function score = feature_score(rbp, obp)
% compute feature score of all feautures on the object
% for circular objects only. for radius computation
% rbp - robot pose
% obp - object pose, estimated one
% f   - feature position w.r.t. the object
% r   - the radius of the object
global conf

score = zeros(size(conf.feats, 1), 1);

for i = 1:size(conf.feats, 1)
    p1 = rbp(1:2);
    p2 = [conf.feats(i, :) 1];
    rtmat = [ cos(obp(3)) -sin(obp(3)) obp(1); ... 
              sin(obp(3)), cos(obp(3)), obp(2); ... 
              0, 0, 1 ];
    p2 = (rtmat*p2')';
    if ~intersection_pts(p1, p2(1:2), obp(1:2), conf.objRad)    % check occlusion
        % check FoV
        n1 = [cos(rbp(3)+pi/2), sin(rbp(3)+pi/2)];
        % compute ori from robot to feature
        n2 = [p2(1)-p1(1), p2(2)-p1(2)];
        ang = acos(dot(n1,n2)/(norm(n2)));
        if ang < conf.FOV/2;
            % compute the score
            nf = [p2(1)-obp(1), p2(2)-obp(2)];
            drc = -n2;
            angf = acos(dot(nf, drc)/(norm(nf)*norm(drc)));
            dstf = norm(drc);
            score(i) = feature_rep(angf, dstf);
        end       
    end
end
