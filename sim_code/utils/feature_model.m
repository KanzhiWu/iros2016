% observation model
% sigma_o: orientation sigma
% base_o : orientation of the feature
% sigma_d: distance sigma
% base_d : distance of the feature
% w_o: orientation weight
% w_d: distance weight
% parameters setup
base_o = [1 0];
sigma_o = 45;     % in degree
base_d = 0.5;
sigma_d = 0.2;    % in meter
w_o = 0.9;
w_d = 0.1;

%% start
% partition the x, y spac   e
[X,Y] = meshgrid(-1:.05:1, -1:.05:1);
V = zeros(size(X, 1), size(X, 2));
for j = 1:size(X, 1)
    for i = 1:size(X, 2)
        x = X(j, i); y = Y(j, i);
        v_d = sqrt(x^2+y^2);
        v_d = exp(-(v_d-base_d)^2/(2*sigma_d^2))
        vec = [x y];
        v_a = acos(dot(vec, base_o)/(norm(vec)))*180/pi;
        v_o = exp(-(v_a)^2/(2*sigma_o^2));
        V(j, i) = v_o *v_d;
    end
end
%V = (V - min(V(:)))/(max(V(:)) - min(V(:)));
surf(X,Y,V);