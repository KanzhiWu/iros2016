% configuration script

% robot control model
conf.V = 1;   % upperbound for velocity m%s
conf.W = pi;    % upperbound for angular velocity rad%s
conf.DT = 0.2;  % time step, second

% robot control noise
conf.sigmaV = 0.5; 
conf.sigmaW = pi/6;
conf.Q = [conf.sigmaV^2 0; 0, conf.sigmaW^2];

% observation model
conf.maxRange = 2.0;
conf.minRange = 0.5;
conf.FOV = pi/6;

% observation noise
conf.sigmaX = 0.005;
conf.sigmaY = 0.005;
conf.sigmaZ = 0.005;
conf.R = [conf.sigmaX^2 0; 0 conf.sigmaZ^2];

% ground truth object pose
% circular 2d object
conf.objPose = [0.0 0.0 0.0];
conf.objRad = 0.2;
% init object pose covariance
conf.objSigmaX = 0.1;
conf.objSigmaY = 0.1;
conf.objSigmaO = pi/5;

% original robot start pose
conf.robPose = [0 -0.8 0];

% grid size
conf.gridSize = conf.V*conf.DT/2;
conf.gridSearch = 20;
conf.gridRadSize = conf.W*conf.DT;
conf.width = [-1, 1];
conf.height = [-1, 1];

% generate object features, w.r.t. the object
if ~exist('feats.mat')
    conf.featsn = 50;
    conf.feats = zeros(conf.featsn, 2);
    conf.featsW = zeros(conf.featsn, 1);
    for i = 1:conf.featsn
        % generate random points
        %ranAngle = rand()*2*pi;
        ranAngle = i/conf.featsn*2*pi;
        conf.feats(i, 1) = conf.objRad*cos(ranAngle);
        conf.feats(i, 2) = conf.objRad*sin(ranAngle);
        conf.featsW(i) = 1.0;
    end
    conf.featsW = conf.featsW/sum(conf.featsW);
else
    load('feats');
    conf.feats = feats;
    conf.featsW = featsW;
    conf.featsn = size(conf.feats, 1);
end

% motion/path planning parameter
conf.nSteps = 5;
conf.uAngles = [-conf.W*conf.DT, 0, conf.W*conf.DT];
%conf.uDists = [-conf.V*conf.DT, conf.V*conf.DT];
conf.uDists = [conf.V*conf.DT];

% grid map setup
conf.mapWidthR = [-1 1];
conf.mapHeightR = [-1 1];
conf.mapStep = 0.4;

conf.gridSize = 0.01;
% build gridmap
conf.mapWidth = conf.mapWidthR(2)-conf.mapWidthR(1)
conf.mapHeight = conf.mapHeightR(2)-conf.mapHeightR(1);
conf.gridMap = zeros(conf.mapHeight/conf.gridSize, conf.mapWidth/conf.gridSize);
conf.gridMapWidth = conf.mapWidth/conf.gridSize;
conf.gridMapHeight = conf.mapHeight/conf.gridSize;
% decide the centre of the object
gCentreX = (conf.objPose(1)-conf.mapWidthR(1))/conf.gridSize;
gCentreY = (conf.objPose(2)-conf.mapHeightR(1))/conf.gridSize;
conf.gridMap(gCentreY, gCentreX) = 1;
for y = 1:size(conf.gridMap, 1)
    for x = 1:size(conf.gridMap, 2)
        py = y*conf.gridSize-conf.gridSize/2-conf.mapHeight/2;
        px = x*conf.gridSize-conf.gridSize/2-conf.mapWidth/2;
        pr = sqrt((px-conf.objPose(1))^2+(py-conf.objPose(2))^2);
        if pr <= conf.objRad
            conf.gridMap(y,x) = 1;
        end
    end
end

% estimator
conf.Xbb = 1.0;

