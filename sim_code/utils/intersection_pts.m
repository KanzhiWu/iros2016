function occl = intersection_pts(p1, p2, ctpt, ctr)
% find the intersection points and occlusiion
% - p1: start point from robot
% - p2: end point, feature
% - ctpt: center point of the circle
% - ctr: radius of the circle

% line equation: y = mx+c
m = (p1(2)-p2(2))/(p1(1)-p2(1));
c = p1(2)-m*p1(1);

% circle equation: (x-a)^2 + (y-b)^2 = r^2
a = ctpt(1); b = ctpt(2);
r = ctr;


% equation solve
x = [-(c - (c + a*m + b*m^2 + m*(- a^2*m^2 + 2*a*b*m - 2*a*c*m - b^2 + 2*b*c - c^2 + m^2*r^2 + r^2)^(1/2))/(m^2 + 1))/m, ...
     -(c - (c + a*m + b*m^2 - m*(- a^2*m^2 + 2*a*b*m - 2*a*c*m - b^2 + 2*b*c - c^2 + m^2*r^2 + r^2)^(1/2))/(m^2 + 1))/m];
y = [(c + a*m + b*m^2 + m*(- a^2*m^2 + 2*a*b*m - 2*a*c*m - b^2 + 2*b*c - c^2 + m^2*r^2 + r^2)^(1/2))/(m^2 + 1), ...
     (c + a*m + b*m^2 - m*(- a^2*m^2 + 2*a*b*m - 2*a*c*m - b^2 + 2*b*c - c^2 + m^2*r^2 + r^2)^(1/2))/(m^2 + 1)];

% compute distance
d = sqrt((p1(1)-p2(1))^2+(p1(2)-p2(2))^2);
d1 = sqrt((x(1)-p1(1))^2+(y(1)-p1(2))^2); 
d2 = sqrt((x(2)-p1(1))^2+(y(2)-p1(2))^2);
% str = sprintf('d = %f, d1 = %f, d2 = %f', d, d1, d2);
% disp(str);
e = 0.001;
if d <= d1+e && d <= d2+e
    occl = 0;
else
    occl = 1;
end


