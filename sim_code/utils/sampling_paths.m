function paths = sampling_paths( rbp, obp, obr )
% generate random sampling trajectories
% rbp   - robot init pose
% obp   - object estimate pose
% obr   - object radius

global conf

% compute grid coordinate of robot pose
gRbp(1) = floor((rbp(1)-conf.width(1))/conf.gridsize);
gRbp(2) = floor((rbp(2)-conf.height(1))/conf.gridsize);

startX = conf.width(1)+conf.gridSize/2; 
startY = conf.height(1)+conf.gridSize/2;
[gridx, gridy] = meshgrid( startX:conf.gridsize:conf.width(2), ...
                           startY:conf.gridsize:conf.height(2));
numGrid = numel(gridx);

% compute neighbourhood grids
% decide maximum grid range
gridWidth = size(startX:conf.gridSize:conf.width(2));
gridHeight = size(startY:conf.gridSize:conf.height(2))
minGridx = max(1, gRbp(1)-conf.gridSearch);
maxGridx = min(gridWidht, gRbp(1)+conf.gridSearch);
minGridy = max(1, gRbp(2)-conf.gridSearch);
maxGridy = min(gridHeight, gRbp(2)+conf.gridSearch);
gridLabel = -1.*ones(gridHeight, gridWidth);
gridLabel(gRbp(2), gRbp(1)) = 0;
for y = minGridy:maxGridy 
    for x = minGridx:maxGridx
        if y == gRbp(2) && x == gRbp(1)
            continue;
        end
        g(1) = gridx(y*gridWidth+x);
        g(2) = gridy(y*gridWidth+x);
        gdist = sqrt((g(1)-rbp(1))^2 + (g(2)-rbp(2))^2);
        gridLabel(y, x) = floor(gdist/(conf.V*conf.DT));
    end
end
poses = {};
for i = 1:conf.nSteps+1
    poses{i} = zeros(conf.gridSearch*conf.gridSearch*(2*pi/conf.gridRadSize), 3);
end
poses{1}(1, :) = rbp;
accuStep = ones(5, 1);      % accumulated poses for each step
% generate poses
for y = 1:gridHeight
    for x = 1:gridWidth
        if gridLabel(y,x) ~= -1 && gridLabel(y,x) ~= 0
            iStep = gridLabel(y,x);
            nRad = max(iStep*2+1, 2*pi/conf.gridRadSize);
            if nRad == 2*pi/conf.gridRadSize
                rads = 0:conf.gridRadSize:2*pi;
                for k = 1:nRad
                    ind = accuStep(iStep);
                    poses{iStep}(ind, 1) = gridx(y*gridWidth+x);
                    poses{iStep}(ind, 2) = gridy(y*gridWidth+x);
                    poses{iStep}(ind, 3) = rads(k);
                    accuStep(iStep) = accuStep(iStep)+1;
                end
            else
                nRad2 = floor(nRad/2);
                rads = -nRad2*conf.gridRadSize:conf.gridRadSize:nRad2*conf.gridRadSize;
                for k = 1:nRad
                    ind = accuStep(iStep);
                    poses{iStep}(ind, 1) = gridx(y*gridWidth+x);
                    poses{iStep}(ind, 2) = gridy(y*gridWidth+x);
                    poses{iStep}(ind, 3) = rads(k);
                    accuStep(iStep) = accuStep(iStep)+1;
                end                
            end
        end
    end
end
% remove zero rows
for i = 1:length(poses)
    poses{i}( ~any(poses{i},2), : ) = [];
end

% generate paths
paths = zeros(1000, 1+conf.nSteps, 3);
pind = 1;
for i1 = 1:size(poses{1}, 1)
    path = zeros(1+conf.nSteps, 3);
    path(1, :) = poses{1}(i1, :);
    for i2 = 1:size(poses{2}, 1)
        % check motion possibility
        
        
        for i3 = 1:size(poses{3}, 1)
            
            for i4 = 1:size(poses{4}, 1)
                
                for i5 = 1:size(poses{5}, 1)
                    
                    for i6 = 1:size(poses{6}, 1)
                    end
                    
                end
                
            end
            
        end
        
    end
end






