% rectangular object model
% belvita box

classdef RectObjModel
    properties
        Label % int, 1, 2, ...
        Name  % object name
        Size  % width and height
        Pose  % centre
        PoseCov
        Feats
        NFeats
        FeatsW
    end
end