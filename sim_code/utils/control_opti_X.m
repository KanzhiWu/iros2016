function f = control_opti_X(u, x, sigmax, rbp )

global conf

f = 0;
%% optimisation
threshp = 0.0; % threshold for p, due to the sigmax
threshfov = conf.FOV;
objRadius = conf.objRad;


somegaObj = sqrt(inv(conf.R));


%% term 1: estimation uncertainty
nsteps = conf.nSteps; % size(u0, 1);
nfeats = conf.featsn; %size(feats, 1);
%A = zeros(3+nsteps*nfeats*2, 3);

% transform features
tFeats = zeros(size(conf.feats, 1), 3);
oFeats = conf.feats;
oFeats(:, 3) = 1;
rtmat = [ cos(x(3)) -sin(x(3)) x(1); ... 
          sin(x(3)), cos(x(3)), x(2); ... 
          0, 0, 1 ];
tFeats = (rtmat*(oFeats'))';


% assign values to A
rbpt = rbp(end, :);
A(1:3, :) = sqrt(inv(sigmax));
aind = 4;
for i = 1:nsteps
    % compute H for each step
    % pose 
    rbpt = motion(rbpt, u(i, :));
    ss = zeros(1, nfeats);
    for j = 1:nfeats
        % compute feature uncertainty
        xfo = conf.feats(j, 1); yfo = conf.feats(j, 2);
        %F = [1, 0, -yfo*cos(x(3))-xfo*sin(x(3)); ...
        %     0, 1, xfo*cos(x(3))-yfo*sin(x(3))];
        %$sigmaf = F*sigmax*F';
        %pf = 1/((2*pi)^2*det(sigmaf));
        %if pf > threshp
            % compute observability
            p1 = rbpt(1:2);
            p2 = tFeats(j, :);
            thp = rbpt(3); tho = x(3);
            if ~intersection_pts(p1, p2(1:2), x(1:2), objRadius)    % check occlusion
                % check FoV
                n1 = [cos(rbpt(3)+pi/2), sin(rbpt(3)+pi/2)];
                % compute ori from robot to feature
                n2 = [p2(1)-p1(1), p2(2)-p1(2)];
                ang = acos(dot(n1,n2)/(norm(n2)));
                if ang < threshfov/2;
                    % compute the score
                    nf = [p2(1)-x(1), p2(2)-x(2)];
                    drc = -n2;
                    angf = acos(dot(nf, drc)/(norm(nf)*norm(drc)));
                    dstf = norm(drc);
                    ss(j) = feature_rep(angf, dstf);
                    hjaco = [cos(thp), sin(thp), xfo*(cos(tho)*sin(thp) - cos(thp)*sin(tho)) - yfo*(cos(tho)*cos(thp) + sin(tho)*sin(thp));
                             -sin(thp), cos(thp), xfo*(cos(tho)*cos(thp) + sin(tho)*sin(thp)) + yfo*(cos(tho)*sin(thp) - cos(thp)*sin(tho))];

                    omega = ss(j).*somegaObj;
                    hjaco = omega*hjaco;
                    A(aind:aind+1,:) = hjaco;
                    aind = aind+2;
                end       
            end

        %end
        %aind = aind+2;
    end  
    %f = f+trace(inv(A'*A));
end
%f = -f;
P = A'*A;
f = trace(inv(P));

%% constraints for motion
%uub = kron([0.5, pi], ones(6, 1));
%ulb = -uub;
%uopt = fmincon(f, u0, [], [], [], [], ulb, uub);



