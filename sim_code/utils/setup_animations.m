function h= setup_animations()
% setup animation
h.xt= patch(0,0,'b','xor'); % vehicle true 
h.xv= patch(0,0,'r','xor'); % vehicle estimate
h.pth= plot(0,0,'k.','markersize',2,'erasemode','background'); 
% vehicle path estimate
h.obs= plot(0,0,'r','erasemode','xor'); % observations
h.xf= plot(0,0,'r+','erasemode','xor'); % estimated features
h.cov= plot(0,0,'r','erasemode','xor'); % covariance ellipses
h.pcov = plot(0, 0, 'b', 'erasemode', 'background'); % covariance for poses
h.tf = plot(0, 0, 'g+', 'MarkerSize', 12, 'erasemode', 'xor'); % observed features
h.ff = plot(0, 0, 'r+', 'MarkerSize', 12,'erasemode', 'xor'); % occluded features
%%