function u = idx2control(ind, dir)
% convert index to control v, w
% ind   - index
% dir   - = 3; 3 directions, no backward
%       - = 6; 6 directions, backward
if nargin < 2
    dir = 3;
end

if dir == 3
    switch ind
        case 1
            u = [1, -1];
        case 2
            u = [1, 0];
        case 3
            u = [1, 1];
    end
else
    switch ind
        case 1
            u = [-1, -1];
        case 2
            u = [-1, 0];
        case 3
            u = [-1, 1];
        case 4
            u = [1, -1];
        case 5
            u = [1, 0];
        case 6
            u = [1, 1];
    end
end

%u = zeros(1, 2);  % v, w
%switch ind
%    case 1
%        u = [1 -1];
%    case 2
%        u = [1 0];
%    case 3
%        u = [1 1];
%end