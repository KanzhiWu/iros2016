function p = make_covariance_ellipses_poses(x, P)
N = 50;
inc = 2*pi/N;
phi = 0:inc:2*pi;
lenp = length(x)/3;
p = zeros(2, lenp*(N+2));
ind = 1;
for i = 1:lenp
    ii = ind:ind+N+1;
    jj = (i-1)*3+1; jj = jj:jj+1;
    p(:, ii) = make_ellipse(x(jj), P(jj, jj), 0.1, phi);
    ind = ind+N+2;
end
%%