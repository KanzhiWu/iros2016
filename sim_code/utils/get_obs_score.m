function s = get_obs_score(S)
% compute observation score from observation matrix
% obsMat    - observation matrix

global conf

if isempty(S)
    s = 0;
else
    s = 0;
    for y = 1:size(S, 1)
        sy = 1.0;
        for x = 1:size(S, 2)
            sy = sy*(1-S(y, x));
        end
        sy = 1-sy;
        %s = s+conf.featsW(y)*sy;
        s = s+sy;
    end
end
