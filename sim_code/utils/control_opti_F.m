function f = control_opti_F(u, x, sigmax, rbp, oldObsMat )

global conf

oldS = get_obs_score(oldObsMat);

%% optimisation
%{
threshp = 0.0; % threshold for p, due to the sigmax
thresho = 0.0; % threshold for observation
threshfov = conf.FOV;
objRadius = conf.objRad;

sigmaOx = conf.sigmaX; sigmaOz = conf.sigmaZ;
omegaObj = conf.R;
somegaObj = sqrt(omegaObj);
%}

%% term 1: estimation uncertainty
nsteps = conf.nSteps; % size(u0, 1);
nfeats = conf.featsn; %size(feats, 1);
rbpt = rbp(end, :);
obsMat = zeros(nfeats, size(oldObsMat, 2)+conf.nSteps);
obsMat(:, 1:size(oldObsMat, 2)) = oldObsMat;
poses = zeros(conf.nSteps, 3);
for i = 1:nsteps
    % compute H for each step
    % pose 
    rbpt = motion(rbpt, u(i, :));
    poses(i, :) = rbpt;
end
obsMat(:, size(oldObsMat, 2)+1:end) = get_obs_matrix(x, sigmax, poses);
S = get_obs_score(obsMat);
f = -(S-oldS);

%f = -1.0*get_obs_score(obsMat);

%% constraints for motion
%uub = kron([0.5, pi], ones(6, 1));
%ulb = -uub;
%uopt = fmincon(f, u0, [], [], [], [], ulb, uub);



