function [ctrlX0, ctrlF0] = greedy_sampling(rbps, obp, obpCov, f)
% find the optimal init path given current robot pose
% rbp   - current RoBot Pose
% obp   - estimated OBject Pose
% obpCov- estimated object pose covariance

global conf
rbp = rbps(end, :);
allpathsIdx = permn([1:length(conf.uAngles)*length(conf.uDists)], conf.nSteps);
R2 = sqrt(conf.R);
% scores
xScores = zeros(size(allpathsIdx, 1), 1); % state estimation
fScores = zeros(size(allpathsIdx, 1), 1); % object recognition

tFeats = zeros(size(conf.feats, 1), 3);
oFeats = conf.feats;
oFeats(:, 3) = 1;
rtmat = [ cos(obp(3)) -sin(obp(3)) obp(1); ...
          sin(obp(3)) cos(obp(3)) obp(2); ...
          0 0 1];
tFeats = (rtmat*(oFeats)')';

sOmegaObj = sqrt(inv(conf.R));

for i = 1:size(allpathsIdx, 1)
    %H = zeros(3+conf.nSteps*conf.featsn*2, 3);
    H(1:3, :) = sqrt(inv(obpCov));
    find = 4;
    rbpt = rbp;
    S = zeros(conf.featsn, conf.nSteps);
    colli = 0;
    for j = 1:conf.nSteps
        u = idx2control(allpathsIdx(i, j));
        u(1) = conf.V*u(1);
        u(2) = conf.W*u(2);
        rbpt = motion(rbpt, u);
        colli = collision_check(rbpt);
        if colli; break; end;
        for k = 1:size(conf.feats, 1)
            xfo = conf.feats(k, 1); yfo = conf.feats(k, 2);
            p1 = rbpt; p2 = tFeats(k, :);
            xp = p1(1); yp = p1(2); thp = p1(3);
            xo = obp(1); yo = obp(2); tho = obp(3);
            if ~intersection_pts(p1(1:2), p2(1:2), obp(1:2), conf.objRad)
                % check FOV
                n1 = [cos(rbpt(3)+pi/2), sin(rbpt(3)+pi/2)];
                % compute ori from robot to feature
                n2 = [p2(1)-p1(1), p2(2)-p1(2)];
                ang = acos(dot(n1,n2)/(norm(n2)));
                if ang < conf.FOV/2;
                    nf = [p2(1)-obp(1), p2(2)-obp(2)];
                    drc = -n2;
                    angf = acos(dot(nf, drc)/(norm(nf)*norm(drc)));
                    dstf = norm(drc);
                    obsVal = feature_rep(angf, dstf);
                    S(k, j) = obsVal;
                    hjaco = [cos(thp), sin(thp), xfo*(cos(tho)*sin(thp) - cos(thp)*sin(tho)) - yfo*(cos(tho)*cos(thp) + sin(tho)*sin(thp));
                             -sin(thp), cos(thp), xfo*(cos(tho)*cos(thp) + sin(tho)*sin(thp)) + yfo*(cos(tho)*sin(thp) - cos(thp)*sin(tho))];
                    omega = obsVal.*sOmegaObj;
                    hjaco = omega*hjaco;
                    H(find:find+1,:) = hjaco;            
                    find = find+2;
                end
            end
        end
    end
    xScores(i) = trace(inv(H'*H));

    if ~colli
        fScores(i) = get_obs_score(S);
    end
    if mod(i, 50) == 1
        disp(['Current iteration is : ' num2str(i)]);
    end
end

% find the best path w.r.t. x
[xmin, xind] = min(xScores);
% compute the path w.r.t. xind
xPoses = zeros(conf.nSteps+1, 3);
xPoses(1, :) = rbp;
xRobCurPose = rbp;
ctrlX0 = zeros(conf.nSteps, 2);

for j = 1:conf.nSteps
    u = idx2control(allpathsIdx(xind, j));
    u(1) = conf.V*u(1);
    u(2) = conf.W*u(2);
    ctrlX0(j, :) = u;
    xRobCurPose = motion(xRobCurPose, u);
    xPoses(j+1, :) = xRobCurPose;
end
% visualise initial path
update_figure(f, obp, obpCov, xPoses, 'g--');

[fmax, find] = max(fScores);
fPoses = zeros(conf.nSteps+1, 3);
fPoses(1, :) = rbp;
fRobCurPose = rbp;
ctrlF0 = zeros(conf.nSteps, 2);

for j = 1:conf.nSteps
    u = idx2control(allpathsIdx(find, j));
    u(1) = conf.V*u(1);
    u(2) = conf.W*u(2);
    ctrlF0(j, :) = u;
    fRobCurPose = motion(fRobCurPose, u);
    fPoses(j+1, :) = fRobCurPose;
end
% control_visualisation(f, obp, obpCov. rbp, ctrlF0, 'green');
% visualise initial path
update_figure(f, obp, obpCov, fPoses, 'r--');


