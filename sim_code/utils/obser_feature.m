function z = obser_feature( Xt, Rt, acc )
% z     - observation nx2
% Xt    - object pose
% Rt    - robot pose
if nargin == 2
    acc = 1;
end

global conf

z = zeros(conf.featsn, 3);
xo = Xt(1); yo = Xt(2); tho = Xt(3);
xp = Rt(1); yp = Rt(2); thp = Rt(3);
Gwo = [cos(tho) -sin(tho) xo; sin(tho) cos(tho) yo; 0 0 1];

Gpw = [cos(thp) sin(thp) -cos(thp)*xp+sin(thp)*yp; ...
       -sin(thp) cos(thp) sin(thp)*xp-cos(thp)*yp; ...
       0 0 1];
F = conf.feats;
F(:, 3) = 1;
z = (Gpw*Gwo*F')';
z(:, 3) = [];
if acc == 0
    for i = 1:size(z, 1)
        z(i, 1) = z(i, 1)+normrnd(0, conf.sigmaX);
        z(i, 2) = z(i, 2)+normrnd(0, conf.sigmaZ);
    end
end



wFeats = zeros(size(conf.feats, 1), 3);   % features in world cooridnate, estimated pose
wFeats = (Gwo*(F)')';
for i = 1:conf.featsn
    if intersection_pts(Rt(1:2), wFeats(i, 1:2), Xt(1:2), conf.objRad)
        z(i, :) = 0;
    end        % check FoV
    n1 = [cos(Rt(3)+pi/2), sin(Rt(3)+pi/2)];
    % compute ori from robot to feature
    n2 = [wFeats(i, 1)-Rt(1), wFeats(i, 2)-Rt(2)];
    ang = acos(dot(n1,n2)/(norm(n2)));
    if ang >= conf.FOV/2;
        z(i, :) = 0;
    end
end


