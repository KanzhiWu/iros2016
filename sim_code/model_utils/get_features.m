function F = get_features(f, objsize, imsize, ind)
% compute features for objece model
% convert sift features in <u,v> to object coordinate system
% imsize    - [<width>, height]
% size      - [width, height]
%   _______________
%  |      w       |　    ｜y
%  |      .       | h    |----->x
%  |              |
%  |______________|
F = zeros(size(f, 2), 3);
switch ind
    case 1
        % front view
        bx = -objsize(1)/2;
        by = -objsize(2)/2;
        for i = 1:size(f, 2)
            fi = f(:, i);
            F(i, 1) = bx+fi(1)/imsize(1)*objsize(1);
            F(i, 2) = by;
            F(i, 3) = fi(3);
        end
    case 2
        % right view
        bx = objsize(1)/2;
        by = -objsize(2)/2;
        for i = 1:size(f, 2)
            fi = f(:, i);
            F(i, 1) = bx;
            F(i, 2) = by+fi(1)/imsize(1)*objsize(2);
            F(i, 3) = fi(3);
        end
    case 3
        % back view
        bx = objsize(1)/2;
        by = objsize(2)/2;
        for i = 1:size(f, 2)
            fi = f(:, i);
            F(i, 1) = bx-fi(1)/imsize(1)*objsize(1);
            F(i, 2) = by;
            F(i, 3) = fi(3);
        end
    case 4
        % left view
        bx = -objsize(1)/2;
        by = objsize(2)/2;
        for i = 1:size(f, 2)
            fi = f(:, i);            
            F(i, 1) = bx;
            F(i, 2) = by-fi(1)/imsize(1)*objsize(2);
            F(i, 3) = fi(3);
        end
end

