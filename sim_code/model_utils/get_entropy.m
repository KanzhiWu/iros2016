function [e, c] = get_entropy(v, n)
% compute entropy
e = 0;
for i = 1:n
    c(i) = length(find(v == i)); % count the repeatance
    c(i) = c(i)/length(v);
    e = e + c(i)*log(c(i));
end
e = -e;



