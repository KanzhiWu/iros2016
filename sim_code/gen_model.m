% generate object model using SIFT features
% for objects: belvita
% rectangle objects only
clear;
clc;
% run vlfeat toolbox
run('vlfeat-0.9.20/toolbox/vl_setup.m');

dataDir = '../data/belvita/';
[cateNames, cateLabels, impaths] = get_im_label(dataDir);

% input object size
if exist('objSize.mat')
    load('objSize');
else
    for i = 1:length(cateNames)
        objSize(i, :) = input_obj_size(cateNames{i});
    end
    save('objSize', 'objSize');
end

it = 1;
featsLabel = zeros(1, length(cateLabels)*100);
descsAll = zeros(128, length(cateLabels)*100);
Fs = {};
for i = 1:length(cateNames)
    itCate = 1;
    Fs{i} = zeros(200, 4);
    for j = 1:length(impaths{i})
        p = impaths{i}{j};
        I = imread(p);
        if size(I, 3) ~= 1
            I = single(rgb2gray(I));
        else
            I = single(I);
        end
        [f, d] = vl_sift(I);
        % assign feature label
        featsLabel(it:it+size(f,2)-1) = i.*ones(1, size(f,2));
        % assign descriptors
        descsAll(:, it:it+size(f,2)-1) = d;
        Fs{i}(itCate:itCate+size(f,2)-1, 1:3) = get_features(f, objSize(i, :), size(I'), j);
        itCate = itCate + size(f,2);
        it = it+size(f,2);
    end
    Fs{i}( ~any(Fs{i},2), : ) = []; % remove 0 rows
end
featsLabel(featsLabel == 0) = [];
descsAll( :, ~any(descsAll,1) ) = [];
% k-means cluster
nwords = floor(size(descsAll, 2)/10);
[kmLabels, kmC] = kmeans(descsAll', nwords);

% compute the entropy for different words
entrs = zeros(nwords, 1);
probs = zeros(nwords, length(cateNames));
featWs = zeros(size(featsLabel, 2), 1);
for i = 1:nwords
    wInds = find(kmLabels == i);% word indices == i
    cateInds = featsLabel(wInds); % category indices 
    [entrs(i), probs(i, :)] = get_entropy(cateInds, length(cateNames));
    for j = 1:length(wInds)
        featWs(wInds(j)) = probs(j);
    end
end

% assign weight to Fs
it = 1;
for i = 1:length(Fs)
    for j = 1:size(Fs{i})
        Fs{i}(j, 4) = featWs(it);
        it = it+1;
    end
    feats = Fs{i};
    save([cateNames{i} '.mat'], 'feats');
end



