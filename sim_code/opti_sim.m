% simulation using random init paths and optimisation

clear all;
close all;
clc;

addpath('./utils/');
disp('Load configuration data and visualisation ...');
global conf
run conf_script

% display init
f = figure;
xinit = conf.objPose;
%xinit = [rand()*conf.objSigmaX rand()*conf.objSigmaY rand()*conf.objSigmaO];
xinitCov = [conf.objSigmaX^2, 0, 0; 0, conf.objSigmaY^2, 0; 0, 0, conf.objSigmaO^2];
robinitPose = conf.robPose;
update_figure(f, xinit, xinitCov, robinitPose, 'b');

%% compute optimal init path
disp('Initial path options:');
disp('1. Greedy search for nearest n steps');
disp('2. Sampling paths in Cartesion space');
%pathOption = input('select an option(default = 1):');

disp('Computing initial path ...');

% generate possible paths by random sampling
% poses = sampling_paths( conf.robPose, xinit, conf.objRad );


allpathsIdx = permn([1:length(conf.uAngles)*length(conf.uDists)], conf.nSteps);
rbpt = conf.robPose;
R2 = sqrt(conf.R);

% scores
xScores = zeros(size(allpathsIdx, 1), 1); % state estimation
fScores = zeros(size(allpathsIdx, 1), 1); % object recognition

% transform features
tFeats = zeros(size(conf.feats, 1), 3);
oFeats = conf.feats;
oFeats(:, 3) = 1;
rtmat = [ cos(xinit(3)) sin(xinit(3)) xinit(1); ... 
          -sin(xinit(3)), cos(xinit(3)), xinit(2); ... 
          0, 0, 1 ];
tFeats = (rtmat*(oFeats'))';
omegaObj = conf.R;
somegaObj = sqrt(omegaObj);
tic;
for i = 1:size(allpathsIdx, 1)
    %H = zeros(3+conf.nSteps*conf.featsn*2, 3);
    H(1:3, :) = sqrt(xinitCov);
    find = 4;
    % path
    poses = zeros(conf.nSteps+1, 3);
    poses(1, :) = conf.robPose;
    rbpt = conf.robPose;
    S = zeros(conf.featsn, conf.nSteps);
    colli = 0;
    for j = 1:conf.nSteps
        u = idx2control(allpathsIdx(i, j));
        u(1) = conf.V*u(1);
        u(2) = conf.W*u(2);
        rbpt = motion(rbpt, u);
        colli = collision_check(rbpt);
        if colli
            break;
        end
        poses(j+1, :) = rbpt;        
        for k = 1:size(conf.feats, 1)
            xf = conf.feats(k, 1); yf = conf.feats(k, 2);
            p1 = rbpt;
            p2 = tFeats(k, :);
            if ~intersection_pts(p1(1:2), p2(1:2), xinit(1:2), conf.objRad)
                % check FoV
                n1 = [cos(rbpt(3)+pi/2), sin(rbpt(3)+pi/2)];
                % compute ori from robot to feature
                n2 = [p2(1)-p1(1), p2(2)-p1(2)];
                ang = acos(dot(n1,n2)/(norm(n2)));
                if ang < conf.FOV/2;
                    % compute the score
                    nf = [p2(1)-xinit(1), p2(2)-xinit(2)];
                    drc = -n2;
                    angf = acos(dot(nf, drc)/(norm(nf)*norm(drc)));
                    dstf = norm(drc);
                    obsVal = feature_rep(angf, dstf);
                    S(k, j) = obsVal;
                    x = xinit;
                    hjaco = [cos(rbpt(3)) -sin(rbpt(3)) ...
                             xf*(cos(x(3))*sin(rbpt(3))-cos(rbpt(3))*sin(x(3)))+yf*(cos(x(3))*cos(rbpt(3))+sin(x(3))*sin(rbpt(3))); ...
                             sin(rbpt(3)) cos(rbpt(3)) ...
                             yf*(cos(x(3))*sin(rbpt(3))-cos(rbpt(3))*sin(x(3)))-xf*(cos(x(3))*cos(rbpt(3))+sin(x(3))*sin(rbpt(3)))];
                    omega = obsVal.*somegaObj;
                    hjaco = omega*hjaco;
                    H(find:find+1,:) = hjaco;
                end
            end
            find = find+2;
        end
        xScores(i) = xScores(i)+trace(H'*H);
    end
    if ~colli
        %update_figure(f, xinit, xinitCov, poses, 'b');
        
        %xScores(i) = trace(H'*H);

        % compute object recognition score
        obsMat = get_obs_matrix(xinit, xinitCov, poses);
        fs = 0.0;
        for y = 1:size(obsMat, 1)
            fsy = 1.0;
            for x = 1:size(obsMat, 2)
                fsy = fsy*(1-obsMat(y, x));
            end
            fsy = 1-fsy;
            fs = fs + conf.featsW(y)*fsy;
        end
        fScores(i) = fs;
    end

    if mod(i, 100) == 1
        disp(['Current iteration is : ' num2str(i)]);
        %update_figure(f, xinit, xinitCov, poses, 'b');
    end
end
initTime = toc;
fprintf('Find the initialisation path takes %f second\n', initTime);

%% Optimisation for pose uncertainty

% find the best path w.r.t. x
[xmin, xind] = max(xScores);
% compute the path w.r.t. xind
xPoses = zeros(conf.nSteps+1, 3);
xPoses(1, :) = conf.robPose;
xRobCurPose = conf.robPose;
ctrlX0 = zeros(conf.nSteps, 2);

for j = 1:conf.nSteps
    u = idx2control(allpathsIdx(xind, j));
    u(1) = conf.V*u(1);
    u(2) = conf.W*u(2);
    ctrlX0(j, :) = [u(1), u(2)];
    xRobCurPose = motion(xRobCurPose, u);
    xPoses(j+1, :) = xRobCurPose;
end
% visualise initial path
update_figure(f, xinit, xinitCov, xPoses, 'b');


uub = kron([conf.V, conf.W], ones(conf.nSteps, 1));
ulb = kron([0, -conf.W], ones(conf.nSteps, 1));

uub = 1.01.*uub;
ulb = 1.01.*ulb;

disp('Optimisation starts ...');
disp('For pose estimation uncertainty');
tic;
options = optimoptions('fmincon','Algorithm','interior-point', 'Display', 'iter', 'MaxIter', 1000);
[uOptX, uOptValX] = fmincon(@(uOptX)control_opti_X(uOptX, xinit, xinitCov, conf.robPose), ctrlX0, [], [], [], [], ulb, uub, [], options);
xOptTime = toc;
fprintf('Pose estimation optimisation takes %f second\n', xOptTime);

% draw optimised poses
xOptPoses = zeros(conf.nSteps+1, 3);
xOptPoses(1, :) = conf.robPose;
xOptRobCurPose = conf.robPose;

for j = 1:conf.nSteps
    u(1) = uOptX(j, 1);
    u(2) = uOptX(j, 2);
    xOptRobCurPose = motion(xOptRobCurPose, u);
    xOptPoses(j+1, :) = xOptRobCurPose;
end
update_figure(f, xinit, xinitCov, xOptPoses, 'green');


%%

[fmax, find] = max(fScores);
% fprintf('The trace: %f\n', xmin);
% compute the path w.r.t. xind
fPoses = zeros(conf.nSteps+1, 3);
fPoses(1, :) = conf.robPose;
fRobCurPose = conf.robPose;
ctrlF0 = zeros(conf.nSteps, 2);

for j = 1:conf.nSteps
    u = [1 0]; %idx2control(allpathsIdx(find, j));
    u(1) = conf.V*u(1);
    u(2) = conf.W*u(2);
    ctrlF0(j, :) = [u(1), u(2)];
    fRobCurPose = motion(fRobCurPose, u);
    fPoses(j+1, :) = fRobCurPose;
end
% visualise initial path
update_figure(f, xinit, xinitCov, fPoses, 'black');

%% Optimisation for recognition coverage
disp('For object recognition coverage');
fPreObsMat = get_obs_matrix(xinit, xinitCov, conf.robPose); 
tic;
[uOptF, uOptValF] = fmincon(@(uOptF)control_opti_F(uOptF, xinit, xinitCov, conf.robPose, fPreObsMat), ctrlF0, [], [], [], [], ulb, uub, [], options);
fOptTime = toc;
fprintf('Object recognition optimisation takes %f second\n', fOptTime);


% draw optimised poses
fOptPoses = zeros(conf.nSteps+1, 3);
fOptPoses(1, :) = conf.robPose;
fOptRobCurPose = conf.robPose;

for j = 1:conf.nSteps
    u(1) = uOptF(j, 1);
    u(2) = uOptF(j, 2);
    fOptRobCurPose = motion(fOptRobCurPose, u);
    fOptPoses(j+1, :) = fOptRobCurPose;
end
update_figure(f, xinit, xinitCov, fOptPoses, 'red');


