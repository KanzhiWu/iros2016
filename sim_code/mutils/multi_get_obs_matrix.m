function S = multi_get_obs_matrix(xs, sigmaxs, poses)
% compute the observation matrix for all features
% x     - state of the object
% sigmax- object pose covariance
% poses - robot poses;

global conf

threshp = 0.0; % threshold for p, due to the sigmax
thresho = 0.0; % threshold for observation
threshfov = conf.FOV;

sigmaOx = conf.sigmaX; sigmaOz = conf.sigmaZ;
omegaObj = conf.R;
somegaObj = sqrt(inv(omegaObj));

S = {};
for i = 1:size(poses, 1)
    % pose 
    rbpt = poses(i, :);
    for k = 1:length(conf.objs)
        obj = conf.objs{k};
        ss = zeros(1, size(obj.Feats, 1));
        x = xs(k, :);
        rtmat = [ cos(x(3)) -sin(x(3)) x(1); ... 
                  sin(x(3)), cos(x(3)), x(2); ... 
                  0, 0, 1 ];
        sigmax = squeeze(sigmaxs(k, :, :));
        ss = zeros(1, size(obj.Feats, 1));
        for j = 1:size(obj.Feats, 1)
            xf = obj.Feats(j, 1); yf = obj.Feats(j, 2);
            F = [1, 0, -yf*cos(x(3))-xf*sin(x(3));
                 0, 1, xf*cos(x(3))-yf*sin(x(3))];
            sigmaf = F*sigmax*F';
            pf = 1/((2*pi)^2*det(sigmaf));
            if pf > threshp
                p1 = rbpt(1:2);
                p2 = (rtmat*[xf; yf; 1])';
                if ~~intersection_pts(p1, p2(1:2), x(1:2), obj.Rad) 
                    n1 = [cos(rbpt(3)+pi/2), sin(rbpt(3)+pi/2)];
                    n2 = [p2(1)-p1(1), p2(2)-p1(2)];
                    ang = acos(dot(n1, n2)/(norm(n2)));
                    if ang < conf.FOV/2
                        nf = [p2(1)-x(1), p2(2)-x(2)];
                        drc = -n2;
                        angf = acos(dot(nf, drc)/(norm(nf)*norm(drc)));
                        dstf = norm(drc);
                        ss(j) = feature_rep(angf, dstf);
                    end
                end

            end
        end
        S{k}(:, i) = ss';
    end
end