% configuration script

% robot control model
conf.V = 1;   % upperbound for velocity m%s
conf.W = pi;    % upperbound for angular velocity rad%s
conf.DT = 0.2;  % time step, second

% robot control noise
conf.sigmaV = 0.2; 
conf.sigmaW = pi/6;
conf.Q = [conf.sigmaV^2 0; 0, conf.sigmaW^2];

% observation model
conf.maxRange = 2.0;
conf.minRange = 0.5;
conf.FOV = pi/3;

% observation noise
conf.sigmaX = 0.005;
conf.sigmaY = 0.005;
conf.sigmaZ = 0.005;
conf.R = [conf.sigmaX^2 0; 0 conf.sigmaZ^2];


% load circular object model
run ObjModel
conf.nObj = 2;

% init object pose covariance
conf.objSigmaX = 0.01;
conf.objSigmaY = 0.01;
conf.objSigmaO = pi/10;

conf.objs{1} = ObjModel;
conf.objs{1}.Label = i;
conf.objs{1}.Rad = 0.12;
conf.objs{1}.Pose = [-0.15 -0.15 pi/4];
conf.objs{1}.PoseCov = [conf.objSigmaX^2, 0, 0;
                        0, conf.objSigmaY^2, 0;
                        0, 0, conf.objSigmaO^2];
conf.objs{1}.NFeats = 25;
conf.objs{1}.Feats = zeros(conf.objs{1}.NFeats, 2);
conf.objs{1}.FeatsW = zeros(conf.objs{1}.NFeats, 1);
for i = 1:conf.objs{1}.NFeats
    ranAngle = rand()*2*pi;
    conf.objs{1}.Feats(i, :) = [conf.objs{1}.Rad*cos(ranAngle), ...
                          conf.objs{1}.Rad*sin(ranAngle)];
    conf.objs{1}.FeatsW(i) = 1.0;
end
conf.objs{1}.FeatsW = conf.objs{1}.FeatsW/sum(conf.objs{1}.FeatsW);

conf.objs{2} = ObjModel;
conf.objs{2}.Label = i;
conf.objs{2}.Rad = 0.12;
conf.objs{2}.Pose = [0.1 0.1 pi/4];
conf.objs{2}.PoseCov = [conf.objSigmaX^2, 0, 0;
                        0, conf.objSigmaY^2, 0;
                        0, 0, conf.objSigmaO^2];
conf.objs{2}.NFeats = 25;
conf.objs{2}.Feats = zeros(conf.objs{1}.NFeats, 2);
conf.objs{2}.FeatsW = zeros(conf.objs{1}.NFeats, 1);
for i = 1:conf.objs{1}.NFeats
    ranAngle = rand()*2*pi;
    conf.objs{2}.Feats(i, :) = [conf.objs{1}.Rad*cos(ranAngle), ...
                          conf.objs{1}.Rad*sin(ranAngle)];
    conf.objs{2}.FeatsW(i) = 1.0;
end
conf.objs{2}.FeatsW = conf.objs{1}.FeatsW/sum(conf.objs{1}.FeatsW);

% original robot start pose
conf.robPose = [0 -0.8 0];

% grid size
conf.gridSize = conf.V*conf.DT/2;
conf.gridSearch = 20;
conf.gridRadSize = conf.W*conf.DT;
conf.width = [-1, 1];
conf.height = [-1, 1];


% motion/path planning parameter
conf.nSteps = 5;
conf.uAngles = [-conf.W*conf.DT, 0, conf.W*conf.DT];
%conf.uDists = [-conf.V*conf.DT, conf.V*conf.DT];
conf.uDists = [conf.V*conf.DT];

% grid map setup
conf.mapWidthR = [-1 1];
conf.mapHeightR = [-1 1];
conf.mapStep = 0.4;

conf.gridSize = 0.01;
% build gridmap
conf.mapWidth = conf.mapWidthR(2)-conf.mapWidthR(1)
conf.mapHeight = conf.mapHeightR(2)-conf.mapHeightR(1);
conf.gridMap = zeros(conf.mapHeight/conf.gridSize, conf.mapWidth/conf.gridSize);
conf.gridMapWidth = conf.mapWidth/conf.gridSize;
conf.gridMapHeight = conf.mapHeight/conf.gridSize;
% decide the centre of the object
for i = 1:length(conf.objs)
    gCentreX = (conf.objs{i}.Pose(1)-conf.mapWidthR(1))/conf.gridSize;
    gCentreY = (conf.objs{i}.Pose(2)-conf.mapHeightR(1))/conf.gridSize;
    conf.gridMap(gCentreY, gCentreX) = 1;
end

for y = 1:size(conf.gridMap, 1)
    for x = 1:size(conf.gridMap, 2)
        py = y*conf.gridSize-conf.gridSize/2-conf.mapHeight/2;
        px = x*conf.gridSize-conf.gridSize/2-conf.mapWidth/2;
        pr1 = sqrt((px-conf.objs{1}.Pose(1))^2+(py-conf.objs{1}.Pose(2))^2);
        pr2 = sqrt((px-conf.objs{2}.Pose(1))^2+(py-conf.objs{2}.Pose(2))^2);
        if pr1 <= conf.objs{1}.Rad || pr2 <= conf.objs{2}.Rad
            conf.gridMap(y,x) = 1;
        end
    end
end

% estimator
conf.Xbb = 1.0;

