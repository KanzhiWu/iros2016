function f = multi_control_opti_F(u, xs, sigmaxs, rbp, oldObsMat )

global conf

oldS = 0;
for i = 1:length(oldObsMat)
    oldS = get_obs_score(oldObsMat{i});
end

%% optimisation

%% term 1: estimation uncertainty
nsteps = conf.nSteps; % size(u0, 1);
rbpt = rbp(end, :);

for i = 1:length(oldObsMat)
    obsMat{i} = zeros(size(conf.objs{i}.Feats, 1), size(oldObsMat{i}, 2)+conf.nSteps);
    obsMat{i}(:, 1:size(oldObsMat{i}, 2)) = oldObsMat{i};
end

poses = zeros(conf.nSteps, 3);
for i = 1:nsteps
    % compute H for each step
    % pose 
    rbpt = motion(rbpt, u(i, :));
    poses(i, :) = rbpt;
end
S = 0;
obsMatNew = multi_get_obs_matrix(xs, sigmaxs, poses);
for i = 1:length(oldObsMat)
    x = xs(i, :);
    obsMat{i}(:, size(oldObsMat{i}, 2)+1:end) = obsMatNew{i};
    s(i) = get_obs_score(obsMat{i});
    S = S+s(i);
end
f = -(S-oldS);
%S = get_obs_score(obsMat);
%$f = -(S-oldS);