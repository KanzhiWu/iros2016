function h = multi_update_figure(f, xs, sigmaxs, poses, robColor)
% update the visualisation image
% f     - figure
% x     - estimated object pose
% sigmax- estimated object pose covariance
% poses - robot poses


% clf(f);

global conf



% setup display properties
xlabel('metres'), ylabel('metres');
set(f, 'name', 'Active Object Detection Simulator');
axis([conf.mapWidthR(1) conf.mapWidthR(2) conf.mapHeightR(1) conf.mapHeightR(2)])
set(gca,'xtick',[conf.mapWidthR(1):conf.mapStep:conf.mapWidthR(2)]);
set(gca,'ytick',[conf.mapHeightR(1):conf.mapStep:conf.mapHeightR(2)]);
axis square

% draw estimated object pose
for i = 1:length(conf.objs)
    % ground truth object pose and object radius
    tX = conf.objs{i}.Pose;
    objRad = conf.objs{i}.Rad;

    % draw the ground truth object pose
    ang = 0:0.01:2*pi;
    tXx = objRad*cos(ang);
    tXy = objRad*sin(ang);
    hold on;
    plot(tX(1)+tXx, tX(2)+tXy, 'c');
    xlabel('metres'), ylabel('metres');

    
    
    x = xs(i, :);
    sigmax = squeeze(sigmaxs(i, :, :));   
    
    hold on;
    plot(x(1), x(2), 'g+');
    plot(x(1)+tXx, x(2)+tXy, 'g--');

    % draw estimated features
    fts = conf.objs{i}.Feats;
    nFts = size(fts, 1);
    rotmat = [ cos(x(3)) -sin(x(3)) x(1); ...
               sin(x(3)) cos(x(3)) x(2); ...
               0, 0, 1];
    covN = 50;
    covInc = 2*pi/covN;
    covPhi = 0:covInc:2*pi;

    for j = 1:nFts
        fo = [fts(j, 1); fts(j, 2); 1];
        fw = rotmat*fo;
        hold on;
        plot(fw(1), fw(2), 'r+');
        % draw covariance
        F = [1, 0, -fo(2)*cos(x(3))-fo(1)*sin(x(3)); ...
             0, 1, fo(1)*cos(x(3))-fo(2)*sin(x(3))];
        sigmaf = F*sigmax*F';
        fellip = make_ellipse(fw, sigmaf, 0.1, covPhi);
        hold on;
        plot(fellip(1,:), fellip(2,:), 'r');
    end
end
% draw robot pose
for i = 1:size(poses, 1)
    poseTri = get_p_tri(poses(i, 1:2), poses(i, 3));
    hold on;
    patch(poseTri(:, 1), poseTri(:, 2), robColor);
end
if size(poses, 1) > 1
    for i = 2:size(poses, 1)
        stpt = poses(i-1, 1:2);
        enpt = poses(i, 1:2);
        hold on;
        plot([stpt(1) enpt(1)], [stpt(2) enpt(2)], robColor);
    end
end
