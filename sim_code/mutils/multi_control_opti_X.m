function f = multi_control_opti_X(u, xs, sigmaxs, rbp )

global conf

f = 0;
somegaObj = sqrt(inv(conf.R));


%% term 1: estimation uncertainty
nsteps = conf.nSteps; % size(u0, 1);
% nfeats = conf.featsn; %size(feats, 1);

% transform features
tFeatsAll = {};
for i = 1:length(conf.objs)
    oFeats = conf.objs{i}.Feats;
    oFeats(:, 3) = 1;
    x = xs(i, :);
    rtmat = [cos(x(3)), -sin(x(3)), x(1); ...
             sin(x(3)), cos(x(3)), x(2);
             0, 0, 1];
    tFeatsAll{i} = (rtmat*(oFeats'))';
end

% assign values to A
for i = 1:length(conf.objs)
    A(3*(i-1)+1:3*(i-1)+3, :) = sqrt(inv(squeeze(sigmaxs(i, :, :))));
end
aind = 3*(length(conf.objs))+1;
rbpt = rbp;
for i = 1:nsteps        
    rbpt = motion(rbpt, u(i, :));
    % compute H for each step
    for k = 1:length(conf.objs)
        x = xs(k, :);
        ss = zeros(1, size(conf.objs{k}.Feats, 1));
        for j = 1:size(conf.objs{k}.Feats, 1)
            xfo = conf.objs{k}.Feats(j, 1);
            yfo = conf.objs{k}.Feats(j, 2);
            p1 = rbpt(1:2); p2 = tFeatsAll{k}(j, :);
            thp = rbpt(3); tho = x(3);
            if ~intersection_pts(p1, p2(1:2), x(1:2), conf.objs{k}.Rad)
                n1 = [cos(rbpt(3)+pi/2), sin(rbpt(3)+pi/2)];
                n2 = [p2(1)-p1(1), p2(2)-p1(2)];
                ang = acos(dot(n1, n2)/norm(n2));
                if ang < conf.FOV/2
                    nf = [p2(1)-x(1), p2(2)-x(2)];
                    drc = -n2;
                    angf = acos(dot(nf, drc)/(norm(nf)*norm(drc)));
                    dstf = norm(drc);
                    obsVal = feature_rep(angf, dstf);
                    hjaco = [cos(thp), sin(thp), xfo*(cos(tho)*sin(thp) - cos(thp)*sin(tho)) - yfo*(cos(tho)*cos(thp) + sin(tho)*sin(thp));
                             -sin(thp), cos(thp), xfo*(cos(tho)*cos(thp) + sin(tho)*sin(thp)) + yfo*(cos(tho)*sin(thp) - cos(thp)*sin(tho))];

                    omega = obsVal*somegaObj;
                    hjaco = omega*hjaco;
                    A(aind:aind+1,:) = hjaco;
                    aind = aind+2;
                end
            end
        end
    end
end
P = A'*A;
f = trace(inv(P));