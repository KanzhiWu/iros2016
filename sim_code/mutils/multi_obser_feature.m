function z = multi_obser_feature( Xts, Rt, acc )
% z     - observation nx2
% Xt    - object pose
% Rt    - robot pose
if nargin == 2
    acc = 1;
end

global conf

featsn = 0;
for i = 1:length(conf.objs)
    featsn = featsn+size(conf.objs{i}.Feats, 1);
end

z = zeros(featsn, 2);
ind = 1;
for i = 1:length(conf.objs)
    xo = Xts(i, 1); yo = Xts(i, 2); tho = Xts(i, 3);
    xp = Rt(1); yp = Rt(2); thp = Rt(3);
    Gwo = [cos(tho), -sin(tho), xo; sin(tho), cos(tho), yo; 0, 0, 1];
    Gpw = [cos(thp), sin(thp), -cos(thp)*xp+sin(thp)*yp;
           -sin(thp), cos(thp), sin(thp)*xp-cos(thp)*yp;
           0, 0, 1];
    F = conf.objs{i}.Feats;
    F(:, 3) = 1;
    Fr = (Gpw*Gwo*F')';
    Fr(:, 3) = [];
    if acc == 0
        for j = 1:size(Fr, 1)
            Fr(j, 1) = Fr(j, 1)+normrnd(0, conf.sigmaX);
            Fr(j, 2) = Fr(j, 2)+normrnd(0, conf.sigmaZ);
        end
    end
    z(ind:ind+size(conf.objs{i}.Feats, 1)-1, :) = Fr;
    %ind = ind+size(conf.objs{i}.Feats, 1);
    
    Fw = (Gwo*(F)')';
    for j = 1:size(conf.objs{i}.Feats)
        if intersection_pts(Rt(1:2), Fw(j, 1:2), Xts(i, 1:2), conf.objs{i}.Rad)
            z(ind+j-1, :) = 0;
        end
        n1 = [cos(Rt(3)+pi/2), sin(Rt(3)+pi/2)];
        n2 = [Fw(j, 1)-Rt(1), Fw(j, 2)-Rt(2)];
        ang = acos(dot(n1, n2)/norm(n2));
        if ang >= conf.FOV/2;
            z(ind+j-1, :) = 0;
        end
    end
    ind = ind+size(conf.objs{i}.Feats, 1);

end



