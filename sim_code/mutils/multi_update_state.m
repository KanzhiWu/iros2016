function [Xts1, XtCovs1] = multi_update_state( Xts, XtCovs, tXts, Rt)
% update state after observing new observation
% Xt1   - state of t+1
% XtCov1- uncertainty of state t+1
% Xt    - state of time t
% XtCov - uncertainty of state time t
% tXt   - ground truth object pose
% Rt    - robot pose



global conf

z = multi_obser_feature(Xts, Rt);
zT = multi_obser_feature(tXts, Rt, 0);
ind = 1;
for i = 1:length(conf.objs)
    labels(ind:ind+size(conf.objs{i}.Feats, 1)-1) = i*ones(size(conf.objs{i}.Feats, 1),1);
    counts(ind:ind+size(conf.objs{i}.Feats, 1)-1) = [1:1:size(conf.objs{i}.Feats, 1)];
    ind = ind+size(conf.objs{i}.Feats, 1);
end

hind = 1;
for i = 1:size(z, 1)
    thp = Rt(3); Xt = Xts(labels(i), :); tho = Xt(3);
    if z(i, 1) ~= 0 && z(i, 2) ~= 0 && zT(i, 1) ~= 0 && zT(i, 2) ~= 0
        xfo = conf.objs{labels(i)}.Feats(counts(i), 1);
        yfo = conf.objs{labels(i)}.Feats(counts(i), 2);
        H(hind, (labels(i)-1)*3+1) = cos(thp);
        H(hind, (labels(i)-1)*3+2) = sin(thp);
        H(hind, (labels(i)-1)*3+3) = xfo*(cos(tho)*sin(thp) - cos(thp)*sin(tho)) + yfo*(cos(tho)*cos(thp) + sin(tho)*sin(thp));
        H(hind+1, (labels(i)-1)*3+1) = -sin(thp);
        H(hind+1, (labels(i)-1)*3+2) = cos(thp);
        H(hind+1, (labels(i)-1)*3+3) = xfo*(cos(tho)*cos(thp) + sin(tho)*sin(thp)) + yfo*(cos(tho)*sin(thp) - cos(thp)*sin(tho));
        hind = hind+2;
    end
end



for i = 1:size(XtCovs, 1)
    XtCov((i-1)*3+1:(i-1)*3+3, (i-1)*3+1:(i-1)*3+3) = squeeze(XtCovs(i, :, :));
end


Q = kron(eye(size(H, 1)/2), conf.R);
K = XtCov*H'*inv(H*XtCov*H'+Q);

z = reshape(z', numel(z), 1);
zT = reshape(zT', numel(zT), 1);

ind = 1;
for i = 1:size(z, 1)
    if  zT(i) ~= 0 && z(i) ~= 0%
        zDiff(ind) = zT(i)-z(i);
        ind = ind+1;
    end
end
Xt1 = (reshape(Xts', 6, 1)+K*zDiff')';
XtCov1 = (eye(6)-K*H)*XtCov;
