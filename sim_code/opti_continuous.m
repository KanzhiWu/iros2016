% simulation for multiple steps path planning


clear all;
close all;
clc;

addpath('./utils/');
disp('Load configuration data and visualisation ...');
global conf
run conf_script

f = figure;

% init pose 
% Xt = [conf.objSigmaX conf.objSigmaY conf.objSigmaO];
Xt = [conf.objPose(1)+normrnd(0, conf.objSigmaX), ...
      conf.objPose(2)+normrnd(0, conf.objSigmaY), ...
      conf.objPose(3)+normrnd(0, conf.objSigmaO)];
% Xt = conf.objPose;
XtCov = [conf.objSigmaX^2, 0, 0; 0, conf.objSigmaY^2, 0; 0, 0, conf.objSigmaO^2];
update_figure(f, Xt, XtCov, conf.robPose, 'k');

% control constraint
uub = kron([conf.V, conf.W], ones(conf.nSteps, 1));
ulb = kron([0, -conf.W], ones(conf.nSteps, 1));

uub = 1.01.*uub;
ulb = 1.01.*ulb;

Rt = conf.robPose;
confirmed = 0;
iter = 1;
while ~confirmed % while the object is not confirmed
    % greedy search for initialisation
    fprintf('Compute initialisation path for step %d.\n', iter);
    tic;
    [ctrlX0, ctrlF0] = greedy_sampling(Rt, Xt, XtCov, f);
    initTime = toc;
    fprintf('Find the initilisation path with %f seconds.\n', initTime)
      
    % decide the next best pose
    disp('Optimisation starts ...');
    disp('For pose estimation uncertainty');
    tic;
    options = optimoptions('fmincon','Algorithm','interior-point', 'Display', 'iter', 'MaxIter', 1000);
    % optimisation based state estimation
    [uOptX, uOptValX] = fmincon(@(uOptX)control_opti_X(uOptX, Xt, XtCov, Rt), ctrlX0, [], [], [], [], ulb, uub, [], options);
    xOptTime = toc;
    fprintf('Pose estimation optimisation takes %f second\n', xOptTime);
    % optimisation based on object recognition
    disp('For object recognition coverage');
    fPreObsMat = get_obs_matrix(Xt, XtCov, Rt); 
    tic;
    [uOptF, uOptValF] = fmincon(@(uOptF)control_opti_F(uOptF, Xt, XtCov, Rt, fPreObsMat), ctrlF0, [], [], [], [], ulb, uub, [], options);
    fOptTime = toc;
    fprintf('Object recognition optimisation takes %f second\n', fOptTime);
    
    % visualise optimised paths
    xOptPoses = zeros(size(Rt, 1)+conf.nSteps, 3);
    fOptPoses = zeros(size(Rt, 1)+conf.nSteps, 3);
    xOptPoses(1:size(Rt, 1), :) = Rt;
    fOptPoses(1:size(Rt, 1), :) = Rt;
    xRt = Rt(end, :); fRt = Rt(end, :);
    for i = 1:conf.nSteps
        xRt = motion(xRt, uOptX(i, :));
        xOptPoses(i+size(Rt, 1), :) = xRt;
        fRt = motion(fRt, uOptF(i, :));
        fOptPoses(i+size(Rt, 1), :) = fRt;
    end
    update_figure(f, Xt, XtCov, xOptPoses, 'g');
    update_figure(f, Xt, XtCov, fOptPoses, 'r');
    
    % decide next poses
    if -uOptValX > conf.Xbb
        u = uOptX(1, :);
    else
        u = uOptF(1, :);
    end
    iter = iter+1;
    % observation update
    Rt(iter, :) = motion(Rt(end, :), u);
    [Xt XtCov] = update_state(Xt, XtCov, conf.objPose, Rt(end, :));
    
    % update figure;
    clf(f);
    update_figure(f, Xt, XtCov, Rt, 'k');



    
    
    
    
    
    
    
end