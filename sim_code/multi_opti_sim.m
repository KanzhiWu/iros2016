% simulation for multiple steps path planning, for multiple objects in scne

clear all;
close all;
clc;

addpath('./utils/');
addpath('./mutils/');

global conf
run multi_conf_script;


% display init
f = figure;
for i = 1:length(conf.objs)
    Xts(i, :) = [conf.objs{i}.Pose(1)+rand()*conf.objSigmaX, ...
                 conf.objs{i}.Pose(2)+rand()*conf.objSigmaY, ...
                 conf.objs{i}.Pose(3)+rand()*conf.objSigmaO];
    XtCovs(i, :, :) = [conf.objSigmaX^2, 0, 0; 0, conf.objSigmaY^2, 0; 0, 0, conf.objSigmaO^2];
end
multi_update_figure(f, Xts, XtCovs, conf.robPose, 'k');
% control constraint
uub = kron([conf.V, conf.W], ones(conf.nSteps, 1));
ulb = kron([0, -conf.W], ones(conf.nSteps, 1));

uub = 1.01.*uub;
ulb = 1.01.*ulb;

Rt = conf.robPose;
confirmed = 0;
iter = 1;


while ~confirmed
    % greedy search for initialisation
    fprintf('Compute initialisation path for step %d.\n', iter);
    tic;
    [ctrlX0, ctrlF0] = multi_greedy_sampling(Rt, Xts, XtCovs, f);
    initTime = toc;
    fprintf('Find the initilisation path with %f seconds.\n', initTime);
    
    % decide the next best pose
    disp('Optimisation starts ...');
    disp('For pose estimation uncertainty');
    tic;
    options = optimoptions('fmincon','Algorithm','interior-point', 'Display', 'iter', 'MaxIter', 1000);
    % optimisation based state estimation
    [uOptX, uOptValX] = fmincon(@(uOptX)multi_control_opti_X(uOptX, Xts, XtCovs, Rt), ctrlX0, [], [], [], [], ulb, uub, [], options);
    xOptTime = toc;
    fprintf('Pose estimation optimisation takes %f second\n', xOptTime);
    % visualise optimised paths
    xOptPoses = zeros(size(Rt, 1)+conf.nSteps, 3);
    xOptPoses(1:size(Rt, 1), :) = Rt;
    xRt = Rt(end, :); 
    for i = 1:conf.nSteps
        xRt = motion(xRt, uOptX(i, :));
        xOptPoses(i+size(Rt, 1), :) = xRt;
    end
    multi_update_figure(f, Xts, XtCovs, xOptPoses, 'g');
    
    % optimisation based on object recognition
    disp('For object recognition coverage');
    fPreObsMat = multi_get_obs_matrix(Xts, XtCovs, Rt); 
    tic;
    [uOptF, uOptValF] = fmincon(@(uOptF)multi_control_opti_F(uOptF, Xts, XtCovs, Rt, fPreObsMat), ctrlF0, [], [], [], [], ulb, uub, [], options);
    fOptTime = toc;
    fprintf('Object recognition optimisation takes %f second\n', fOptTime);
    % visualise optimised paths
    fOptPoses = zeros(size(Rt, 1)+conf.nSteps, 3);
    fOptPoses(1:size(Rt, 1), :) = Rt;
    fRt = Rt(end, :); 
    for i = 1:conf.nSteps
        fRt = motion(fRt, uOptF(i, :));
        fOptPoses(i+size(Rt, 1), :) = fRt;
    end
    multi_update_figure(f, Xts, XtCovs, fOptPoses, 'r');
    
    
    if -uOptValX > conf.Xbb
        u = uOptX(1, :);
    else
        u = uOptF(1, :);
    end
    iter = iter+1;
    % observation update
    Rt(iter, :) = motion(Rt(end, :), u);
    objPoses(1,:) = conf.objs{1}.Pose;
    objPoses(2,:) = conf.objs{2}.Pose;
    [Xts XtCovs] = multi_update_state(Xts, XtCovs, objPoses, Rt(end, :));
    
    % update figure;
    clf(f);
    multi_update_figure(f, Xt, XtCov, Rt, 'k');
end

